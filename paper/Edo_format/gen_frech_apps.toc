\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Background and motivations}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Setting and results}{3}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}More specific motivations}{4}{subsubsection.1.2.1}
\contentsline {section}{\numberline {2}Polyhedral Geometry}{5}{section.2}
\contentsline {section}{\numberline {3}Polyhedral Algebra}{8}{section.3}
\contentsline {section}{\numberline {4}Finding Frechet bounds}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Geometrically removing a potentially redundant $ H_1 $}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Algebraically removing $ H_1 $}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}The Generalized Frechet Bound Algorithm}{13}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Looping through Frechet bounds}{14}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Comments}{15}{subsection.4.5}
\contentsline {section}{\numberline {5}Experiments}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Partially filled contingency tables}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Network tomography}{16}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Basics of network tomography}{16}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Minimum bound data analysis}{19}{subsubsection.5.2.2}
\contentsline {section}{\numberline {6}Conclusion}{19}{section.6}
\contentsline {section}{References}{20}{section*.2}
