\section{Introduction}
\label{sec:intro}
\subsection{Motivations}
Cross-classified categorical data is analyzed in tables called \emph{contingency tables} (see for example \cite{Fienberg2007bk}).
Analyzing the dependence of a given such data set usually requires to compute marginals of this table like row-sums and column-sums for $2$-way tables.
For multiway tables ($k$-way with $ k \geq 2 $), the marginals to compute are determined by the hypothesis of independence (see for example \cite{Dobra2000pap}).
For example with Fisher exact test, one would need to enumerate the tables with those given marginals and compute the p-value of the given table.
More realistically one would uniformly sample a large enough number of these tables to compute an approximate p-value.
Such calculations require some assumption or inference on the distribution of table entries.
In this process it is helpful to have some tight bounds on those entries.
Such bounds, called Frechet bounds, are well-known and are used as well in other contexts, like the theory of copulas (see for example \cite{Joe1997bk}).
In the simpler case of a $2$-way contingency table $(x)_{ij}$, $i = 1,\dots, I $, $ j = 1, \dots, J $, the marginals are $ x_{i+} = \sum_j x_{ij} $ and $ x_{+j} = \sum_i x_{ij} $, and with total count $ N = \sum_{ij} x_{ij} $, the Frechet bounds are
\[
\max(0, x_{i+} + x_{+j} - N) \leq x_{ij} \leq \min(x_{i+}, x_{+j})
\]
Already at this point it is rather natural to ask:
How many entries have a minimum bound greater than $ 0 $.
We answer this question in theorem \ref{thm:nnztmin}.

Beside being very easy to compute, Frechet bounds are sharp.
That is, for every $ i,j $, there is a table where $ x_{ij} $ is equal to the lower bound and there is a table where $ x_{ij} $ is equal to the upper bound.
It turns out that the set of all tables with fixed marginals is a polytope $ P $ of dimension $ (I-1)*(J-1) $ (see section \ref{sec:geomsol}).
So from a geometric perspective, Frechet bounds define a tight bounding box of dimension $ I*J $ that contains $ P $ as illustrated in figure \ref{fig:bound_box}.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.4]{bound_box_new.pdf}}
\caption{Frechet bounds define a tight bounding box for the solution polytope $ \mathcal{P} $.
\label{fig:bound_box}}
\end{center}
\vskip -0.2in
\end{figure} 

However, if the table contains structural zeros, that is, if it is known that some entries are always $ 0 $, or equivalently if the table is partially filled, Frechet bounds fail to be sharp.
All this generalizes without major difficulty to multi-way tables.

Contingency tables with given marginals are solutions of an under-determined linear system
\begin{equation*}
\label{init_syst}
Ax = y \qquad x \in \mathbb{N}^n
\end{equation*}
where $ x $ is the vectorized list of table entries, $y$ is the vectorized list of marginal entries, and $ A $ is the incidence matrix between the table entries and the marginals.
That is, $ A[i,j] = 1 $ if $ x[j] $ contributes to the marginal entry $ y[i] $, and $ A[i,j] = 0 $ otherwise.
Since some marginal entries can be recovered from the rest, some rows can be eliminated so that $ A $ is a full rank $m$ by $n$ matrix.
Many other problems are similar in nature and form a linear sub-class of a larger class called \emph{ill-posed inverse problems}.
They are inverse problems because one tries to infer information about the object (here the table) from observations (marginal counts).
They are ill-posed because there are usually more than one solution (or no solution at all, i.e. the problem is not \emph{feasible}).
Such problems include contingency tables with given marginals, including tables with structural zeros (see \cite{Chen2007pap}), network tomography (see \cite{Vardi1996pap}, \cite{Castro2004pap}), mass transportation problems (see \cite{Rachev1999pap}), networks with given degree vectors (see \cite{Blitzstein2011pap}, \cite{Chatterjee2010pap}), and flux-balance analysis (see \cite{Orth2010prim}).
All these problems have a solution set that is a polytope usually in high dimension.
However, no efficient algorithm has been proposed (to the authors' knowledge) to compute Frechet-like sharp bounds for such general systems.

\subsection{Setting and results}
These general systems of interest to which we apply the notion of (generalized) Fr\'echet bounds are defined as the solution set of
\begin{equation}
\label{eq:Axy_xnn}
Ax = y \qquad x \geq 0
\end{equation}
Where $ A $ is a given $m$ by $n$ matrix, $ m < n $, and $ y $ a given vector (typically, the ``vector of observations'').
Moreover we assume that the system is feasible (clearly it is for the contingency table problem since we are given one solution), and that $ A $ has full rank.
The latter assumption might entail eliminating some rows if necessary:
For example in $2$-way $ I $ by $ J $ tables we know the total number of observations from the sum of the $I$ row-marginals.
Therefore given $J-1$ column marginals one can infer the last one.
That is, for $ A $ to be full rank, its number of rows $m$ is $ I + J - 1 $.

In the contingency table problem, the solutions $ x $ are counts and therefore integers.
Here we generalise to real solutions.

Such a solution set is called a (convex) \emph{polyhedron} (the intersection of finitely many half-spaces).
In most problems it is bounded; it is then called a (convex) \emph{polytope} (see \cite{Fukuda2004lk}).
We will write it $ \mathcal{P}(A,y) $, or simply $ \mathcal{P} $.
For example in the contingency table problem, cell values $ x[i] $ cannot run to infinity.
Our algorithm \ref{algo:getgFb} and theorem \ref{thm:nnztmin} can be adapted to the unbounded case, but for simplicity and greater interest we decided to state and describe them for the bounded case.
Finally the set of solutions to $ Ax = y $ without the constraint $ x \geq 0 $ will be called the \emph{solution space}.
We will write it $ \mathcal{S}(A,y) $, or simply $ \mathcal{S} $.

\begin{define}
A \emph{generalized Fr\'echet bound}, or now simply a \emph{Fr\'echet bound}, is a sharp minimum or maximum bound on the coordinate of the solutions to (\ref{eq:Axy_xnn}).
\end{define}

We can now state our two main results:

\begin{prop}
Algorithm \ref{algo:getgFb} from section \ref{sec:bdalgo} outputs all the Frechet bounds for a general system \ref{eq:Axy_xnn}.
\end{prop}

\begin{thm}
\label{thm:nnztmin}
In system (\ref{eq:Axy_xnn}), there are at most $ m-1 $ minimum Fr\'echet bounds that are positive.
\end{thm}
Notice that since $ x \geq 0 $ all other minimum Fr\'echet bounds are zero.

In section \ref{sec:geomsol} we describe the geometry and the linear algebra of the solution set.
This is sufficient to prove the theorem.
Then we describe in section \ref{sec:linalg} the vertices of the polytope and of the arrangement of its minimal set of supporting hyperplanes, and in section \ref{sec:pivsolsp} how to pass from one to another by pivoting.
We then describe in section \ref{sec:bdalgo} our modified simplex algorithm, more specifically how the determination of allowable pivots at each step of the simplex algorithm is simplified and how it is used to get the Frechet bounds.
Finally section \ref{sec:experim} is devoted to two application examples.

We use the standard notation of the $R$ programming language (which we used to code our applications) for vectors $ v $ and their entries $ v[i] $, and for matrices $ M $ and their rows $ M[i,] $, columns $ M[,j] $, etc.
