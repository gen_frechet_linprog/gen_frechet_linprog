\documentclass{article}
\usepackage{aistats2e}

\usepackage{amsthm,amsmath,amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{afterpage}
\usepackage{lgreek}

%\title{Generalized Frechet bounds through \\specialized linear programming}

%\author{Bertrand Haas, Edoardo Airoldi\\
%Harvard University, Department of Statistics}

\def\keywords{\vspace{.5em}
{\textit{Keywords}:\,\relax%
}}
\def\endkeywords{\par}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{define}[thm]{Definition}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}

\DeclareMathOperator{\interior}{interior}

\graphicspath{{../figures/}}

\begin{document}
\newcommand{\vectwo}[2]{
    \left(\begin{array}{c}
     {#1} \\ {#2}
    \end{array}\right)
}
\newcommand{\sign}{\operatorname{sign}}
\newcommand{\inv}{\operatorname{inv}}
%\maketitle

\twocolumn[

\aistatstitle{Generalized Frechet bounds through \\a simplified simplex algorithm}

\aistatsauthor{XXX \And YYY \And ZZZ}

\aistatsaddress{Department of VVV, UUU University} ]

\begin{abstract}
\input{abstract.tex}
\end{abstract}

%\keywords{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{intro.tex}

\section{The polyhedral geometry of the solution space}
\label{sec:geomsol}
We describe here how the solution space is defined as polytope and use this to prove theorem \ref{thm:nnztmin}.

Recall from section \ref{sec:intro} that we are interested in the system:
\begin{equation}
\label{eq:Axy_xnn}
Ax = y \qquad x \geq 0
\end{equation}
with $ A $ an $m$ by $n$ matrix, $ m < n $.
Moreover we assume that the system is feasible (clearly it is for the contingency table problem since we are given one solution), and that $ A $ has full rank.
The latter assumption might entail eliminating some rows if necessary:
For example in $2$-way $ I $ by $ J $ tables we know the total number of observations from the sum of the $I$ row-marginals.
Therefore given $J-1$ column marginals one can infer the last one.
That is, for $ A $ to be full rank, its number of rows $m$ is $ I + J - 1 $.

Without loss of generalization we can assume that the coordinate to bound is the first one, $ x[1] $.
Let it vary continuously from $ 0 $ on: $ x[1] = t $, $ t \geq 0 $.
For a given $ t $, system \ref{eq:Axy_xnn} becomes $ A'x' = y ' $ where $ A' = A[,2:n] $ is obtained from $ A $ by removing the first column, $ x' = x[2:n] $ is obtained from $ x $ by removing the first coordinate and $ y' = y - t * A[,1] $.
We want to find sharp bounds $ t_\mathrm{min} $ and $ t_\mathrm{max} $, that is, bounds such that for any $ t_\mathrm{min} \leq t \leq t_\mathrm{max} $ the updated system is feasible and for $ t $ outside this range, the updated system is not feasible.

\begin{define}
Given a system $ Mx = b $, the \emph{unconstrained solution space} (or simply the \emph{solution space}) $ \mathcal{S}(M,b) $ (or simply $ \mathcal{S} $) is the set of solutions of that system (without the constraint $ x  \geq 0 $). \\
The \emph{constrained solution space} or the \emph{solution polytope} $ \mathcal{P}(A,y) $ (or simply $ \mathcal{P} $) is the set of solutions of the system with the constraint $ x \geq 0 $.
\end{define}
We are interested here in the solution space $ \mathcal{S} = \mathcal{S}(A,y) $ and the solution polytope $ \mathcal{P} = \mathcal{P}(A,y) $ of the system \ref{eq:Axy_xnn}.
Each row of $ Ax = y $ defines an affine hyperplane.
So $ \mathcal{S} $ is the intersection of these $ m $ independent ($ A $ has full rank) hyperplanes.
This is an affine subspace of dimension $ n-m $.
$ \mathcal{S} $ intersects the non-negative orthant in a polyhedron.
If the solution space is bounded (which we assume here), this is a polytope.
For example if only one row $ A[i,] x = y[i] $ has all entries $ A[i,j] $ of same sign than $ y[i] $, then this hyperplane intersects the non-negative orthant into a simplex, which is clearly bounded.
Therefore any subsequent intersection will also be bounded.

The vertices of the solution polytope are the intersections of $ \mathcal{S} $ with the coordinate planes of dimension $ m $.
Indeed, the dimension of such intersection is $ (n-m) + (m) - n = 0 $.
Therefore vertices have at most $ m $ non-zero (and positive) coordinates.
And reciprocally any solution with $ n-m $ zeros and $ m $ non-negative coordinates is a vertex of the solution polytope.
We call \emph{pseudo-vertex} a point of $ \mathcal{S} $ with at most $ m $ non-zero coordinates (positive or negative).

A hyperplane of coordinates defined by $ x[i] = 0 $ intersects $ \mathcal{S} $ into a hyperplane $ H_i $ (that is, a hyperplane of $ \mathcal{S} $).
The set of all these hyperplanes forms an arrangement of hyperplanes $ \mathcal{H} = \{H_i, i = 1, \dots, n\} $.
Any $ m $ such hyperplanes intersect in a point ($ A $ being full rank, no two hyperplanes are parallel).
Such intersection points are precisely the pseudo-vertices.
Moreover the hyperplanes bound cells (polyhedrons).
These cells are the intersections of the unconstrained solution space with the different orthants and can therefore be labeled by corresponding signs.
In particular the intersection with the non-negative orthant is $ \mathcal{P} $ and is labeled with $ n $ plusses $ (+,+, \dots, +) $ (see figure \ref{fig:bdhyp5}).

Notice that the hyperplane of coordinate $ H_1 $ bounds $ \mathcal{S} $ if and only if $ t_{min} = 0 $.
If $ H_1 $ does not bound $ \mathcal{P} $, then by pushing it continuously to $ H_1(t) = \{x: x[1] = t\} \cap \mathcal{S} $ for $ t \geq 0 $, at some point it will meet $ \mathcal{P} $ and it will do so precisely for the value $ t = t_{min} $.
Whether or not it bounds $ \mathcal{P} $, by pushing it further, it will keep intersecting $ \mathcal{P} $ up until a value $ t = t_{max} $ (see figure \ref{fig:bdhyp5}).

\subsection{Proof of Theorem \ref{thm:nnztmin}}
\label{sec:thm1proof}
Recall that if $ T $ be the number of coordinates $ x[i] $ for which $ t_{min} > 0 $, theorem \ref{thm:nnztmin} states that $ T \leq m-1 $.
\begin{proof}
The minimum number of facets of the solution polytope $ \mathcal{P} $ is $ n-m + 1 $ (that is, $ \mathcal{P} $ is a simplex).
Each such facet is contained in a hyperplane $ H_i $.
Since there are $ n $ such hyperplanes, the maximum number of the ones not bounding $ \mathcal{S} $ is equal to $ n - (n-m+1) = m - 1 $.
Therefore, $ T \leq m-1 $.
\end{proof}

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.4]{bding_hypl_n5.pdf}}
\caption{An example where the unconstrained solution space has dimension $ n-m = 2 $ and $ n = 5 $ hyperplanes of coordinates, and the solution polytope $ \mathcal{S} $ is a triangle.
By pushing $ H_1 $ by $ t_{min} $ the hyperplane intersects $ \mathcal{S} $ for the first time and by $ t_{max} $ for the last time.\label{fig:bdhyp5}}
\end{center}
\vskip -0.2in
\end{figure}

\section{The discrete geometry of the solution space}
\label{sec:linalg}
We describe here the set of points of interest in the solution space, the vertices of the hyperplanes arrangement, that we call \emph{pseudo-vertices} (we reserve the term \emph{vertices} for those of the solution polytope).

Since $ A $ has full rank, we permute with a permutation $ \pi $ the columns to get a block decomposition $ A[,\pi] = (A_1,A_2) $ where $ A_1 $ is a $m$ by $m$ matrix of rank $ m $.
Notice that such a permutation can easily be found for example by using a QR decomposition routine.
For simplicity assume for the remainder of this section that the coordinates have been reordered by $ \pi $, so that $ A = (A_1, A_2) $.
Let $ Q $ be the $n$ by $n$ matrix defined by two or four blocks
\[
Q = (Q_1, Q_2) =
\left(\begin{array}{cc}
Q_{11} & Q_{12} \\
Q_{21} & Q_{22}
\end{array}\right) = \left(\begin{array}{cc}
A_1^{-1} & -A_1^{-1} A_2 \\
0 & I_{n-m}
\end{array}\right)
\]
With $ Q_1 = \vectwo{Q_{11}}{Q_{21}} $, $ Q_{11} = A_1^{-1} $, etc.

One can easily check that\footnote{If $ A $ is integral and unimodular, this is the \emph{Hermite normal decomposition} of $ A $}.
\begin{equation}
\label{eq:hermitedec}
AQ = (I_m , 0)
\end{equation}

Let's re-write the system $ Ax = y $ as $ AQQ^{-1}x = y $, that is, $ (I_m,0) x' = y $ with $ x' = Q^{-1}x $.
Then it becomes easy to find a first solution $ x'^{(0)} = \vectwo{y}{0} $, so $ x^{(0)} = Q_1 y = \vectwo{Q_{11}y}{0} $.
This is a solution in the unconstrained solution space with at most $m$ non-zero coordinates.
Therefore this is a pseudo-vertex.
What we will actually want is a vertex and we will see in section \ref{sec:movsolsp} that the pivoting algorithm developped right below will move from this first pseudo-vertex to a first vertex.

Once we have the pseudo-vertex $ x $, we will see that the important matrix is $ Q_2 $.
\begin{define}
We call the triplet $ (x, Q_2, \pi) $ a \emph{pseudo-vertex triplet}.
\end{define}

\begin{prop}
\label{prop:Q2}
Let $ (x, Q_2, \pi) $ be a pseudo-vertex triplet.
\begin{enumerate}
\item The columns of $ Q_2 $ form a basis of the null space of $ A[,\pi] $.
\item The columns of $ Q_2 $ are directions along the lines of the hyperplanes arrangement $ \mathcal{H} $ at $ x $.
\item The columns of $ Q_2 $ span a simplicial cone at $ x $ that contains the solution polytope.
\end{enumerate}
\end{prop}
\begin{proof}
\mbox{}
\vspace{-1em}
\begin{enumerate}
\item From equation (\ref{eq:hermitedec}) we get that $ A[,\pi] Q_2 = 0 $.
Since $ Q_{22} = I_{n-m} $, The columns of $ Q_2 $ are independent.
Moreover, there are $ n-m $ columns and the null space has dimension $ n-m $.

\item Pseudo-vertex $ x $ is the intersection in $ \mathcal{S} $ of the $ n-m $ hyperplanes defined by $ x[m+1] = \dots = x[n] = 0 $.
For a given $ j $ let $ x_t = x + t Q_2[,j] $ for $ t \geq 0 $ be a moving point.
For $ t > 0 $, it gets a new positive coordinate, namely $ x_t[m + j] = t $, so it has now at most $ m+1 $ non-zero coordinates.
That is, it belongs to the intersection of the remaining $ n-m-1 $ coordinate hyperplanes.
This intersection defines a line of the hyperplanes arrangement.

\item The unconstrained solution space $ \mathcal{S} $ is a translation of the nullspace by the vector $ x $.
So any other point $ x' \in \mathcal{S} $ is uniquely written $ x' = x + \tau Q_2 $.
From the structure of $ Q_2 $ we get that $ \tau = x'[(m+1):n] $.
Now $ x' \in \mathcal{P} $ implies that $ x'[(m+1):n] \geq 0 $.
This shows that $ \tau \geq 0 $, that is, $ x' $ belongs to the cone at $ x $ generated by the columns of $ Q_2 $.
\end{enumerate}
\end{proof}

\begin{define}
\label{def:pivot}
Two pseudo-vertices $ x $ and $ x' $ are \emph{neighbors} if they both lie on the same line of $ \mathcal{H} $.\\
$ x $ and $ x' $ are \emph{adjacent} if they are neighbors and no other neighbor lies between them.\\
\emph{Pivoting} is the operation $ (x, Q_2, \pi) \rightarrow (x', Q_2', \pi') $ where $ x $ and $ x' $ are neighbor along a direction $ Q_2[,j] $.
\end{define}

\section{The network geometry of the solution space}
\label{sec:pivsolsp}
The operation of pivoting defined in \ref{def:pivot} is used to navigate from pseudo-vertex to pseudo-vertex along edges (the lines of the hyperplanes arrangement) thus defining a network.
We describe here the pivoting operation in more details by separating it into a pivoting operation on the pseudo-vertex and a pivoting operation on the null-space basis.
\begin{eqnarray*}
\mathrm{xPivot}(x, Q_2, i, j) &=& x' \\
\mathrm{QPivot}(Q_2,\pi,i,j) &=& (Q'_2,\pi')
\end{eqnarray*}
This might look somewhat artificial but it gives further insight on the role of each index in the pivot $ (i,j) $.

\subsection{Pivoting pseudo-vertices}
We are assume in this section that $ (x, Q_2, \pi) $ is a pseudo-vertex triplet, so the coordinates have been rearanged according to permutation $ \pi $.
\begin{prop}
With input $ (x, Q_2; i, j) $ algorithm \ref{algo:xpivot} returns the pseudo vertex $ x' $ equal to the intersection of the line through $ x $ with direction $ Q_2[,j] $ with the hyperplane defined by $ x[i] = 0 $.
\end{prop}
Beware that in the algorithm the coordinates $ x'[i] $ and $ x'[m+j] $ are permuted with respect to the ones from $ x $ but the permutation $ \pi $ from the vertex triplet is not yet updated (it will be at algorithm \ref{algo:qpivot}).

\begin{algorithm}[h!]
\caption{xPivot($x$,$Q_2$;$i$,$j$)}
\label{algo:xpivot}
\begin{algorithmic}[1]
\STATE $ t \leftarrow -\frac{x[i]}{Q_2[i,j]} $
\STATE $ x \leftarrow x + t Q_2[,j] $ \label{xpivot:newx}
\STATE Swap coordinates $ i $ and $ m + j $ of $ x $ \label{xpivot:swap}
\STATE return $ x $
\end{algorithmic}
\end{algorithm}

\begin{proof}
If at the input $ x[i] = 0 $ then the $ x' = x $ and the algorithm indeed returns $ x $ unchanged (with two zero coordinates swapped).
So assume $ x[i] \not = 0 $ at input time.
After line (\ref{xpivot:newx}:) $ x[m+j] = t $, $ x[m+k] = 0 $ for $ 1 \leq k \leq n-m $, $ k \not = j $, and $ x[i] = 0 $.
So $ x $ has now at most $ m $ non-zero coordinates and is therefore a pseudo-vertex.
In other words, after leaving the hyperplane $ x[m+j] = 0 $, the point $ x_t = x + t Q_2[,j] $ has moved along an edge defined by the remaining $ n-m-1 $ hyperplane until it reached the hyperplane defined by $ x[i] = 0 $.
\end{proof}

\begin{define}
The ordered pair of indexes $ (i,j) $ is called a \emph{pivot}.
\end{define}

\begin{prop}
Let $ Q_2[,j] $ be a given direction from $ x $ and let $ H_k $ be the first hyperplane intersecting the half-line $ (x, Q_2[j]) $ from $ x $ if any. \\
With input $ (x, Q_2, j) $, algorithm \ref{algo:geti} returns $ -1 $ if there is no such $ H_k $; otherwise it returns $ k $.
\end{prop}

\begin{algorithm}[h!]
\caption{get\_i($x$,$Q_2$,$j$)}
\label{algo:geti}
\begin{algorithmic}
\STATE $ I \leftarrow \{i : \sign(x[i]) = -\sign(Q_2[i,j]) \not = 0 \} $
\IF {$ I = \emptyset $}
    \STATE return $-1$
\ELSE
    \STATE $ k \leftarrow \arg\min\left( -\frac{x[i]}{Q_2[i,j]}\right) $ over $ I $ \label{geti:i}
    \STATE return $ k $
\ENDIF
\end{algorithmic}
\end{algorithm}

\begin{proof}
Let $ x_t = x + t Q_2[,j] $, $ t \geq 0 $, be a point moving on the half-line $ (x, Q_2[,j]) $.
There are at most $ m $ hyperplanes $ H_k $ intersecting this line (beside the one defined by $ x[m+j] = 0 $ which we just left).
The next hyperplane intersecting the line is met by $ x_t $ for the first positive value $ t_1 $ where, as $ t $ increases, a coordinate gets cancelled $ x_{t_1}[i] = x[i] + t_1 Q_2[i,j] = 0 $.
So $ t_1 = \min(-x[i] / Q_2[i,j]) $, but since $ t_1 \geq 0 $, either $ t_1 = 0 $ and $ x[i] = 0 $, or the index $ i $ must satisfy $ \sign(x[i]) = - \sign(Q_2[i,j]) $.
\end{proof}

\begin{lem}
\label{lem:nonewneg}
Given $ 1 \leq j \leq n-m $, if $ i = \mathrm{get\_i}(x, Q_2, j) \geq 1 $, then $ x' = \mathrm{xPivot}(x,Q_2,i,j) $ is adjacent to $ x $ and has no more negative coordinates than $ x $.
\end{lem}
\begin{proof}
Let $ x_t = x + t * Q_2[,j] $.
For $ 0 < t < -x[i]/Q_2[i,j] $ the number of positive coordinates of $ x_t $ is one more than that of $ x $ (we have $ x_t[m+j] = t $ and $ x[m+j] = 0 $).
For $ t = -x[i]/Q_2[i,j] $, we get $ x_t[i] = 0 $ (and perhaps $ x_t[k] = 0 $ for $ k \not = i $ as well).
So no new negative coordinate has been created.
\end{proof}

\subsection{Pivoting the null space basis}
\begin{prop}
Given a pseudo-vertex triplet $ (x, Q_2, \pi) $ and a pivot $ (i,j) $, let the pseudo vertex $ x' = xPivot(x, Q_2, i, j) $.
Then the pair $ (Q_2', \pi') $ from the pseudo-vertex triplet $ (x', Q_2', \pi') $ is returned by algorithm \ref{algo:qpivot} with input $ (Q_2, \pi; i, j) $.
\end{prop}

\begin{proof}
At line \ref{qpivot:0out} we use $ Q_2[i,j] $ to zero out the rest of row $ Q_2[i,] $ by adding to each column the appropriate multiple of the column $ Q_2[,j] $.
At the same times this fills in the row $ Q_2[m+j] $.
This is still a null space basis, but the nice structure $ Q_2 = \vectwo{Q_{12}}{I_{n-m}} $ has been altered.
To get back this structure we just need to swap rows $ i $ and $ m+j $ (line \ref{qpivot:rowswap}).
Before swapping, though, we make sure that the $ Q_2[i,j] = 1 $ by dividing $ Q[,j] $ by $ Q_2[i,j] $ (line \ref{qpivot:normalize}).
Notice that this matches the swap for $ x $ at line \ref{xpivot:swap} of algorithm \ref{algo:xpivot}.
The permutation $ \pi $ is updated accordingly at line \ref{qpivot:permupdate}.
Finally notice that the set of such operations on $ Q_2 $ forms a non-degenerate linear transformation and therefore preserve the property of the columns as being a basis of the nullspace.
Moreover, since now $ Q_2[(m+1):n] = I_{n-m} $ such basis is unique, it is equal to the matrix $ Q'_2 $ of the pseudo-vertex triplet $ (x', Q_2', \pi') $.
\end{proof}

\begin{algorithm}
\caption{QPivot($Q_2$, $\pi$, $i$, $j$)}
\label{algo:qpivot}
\begin{algorithmic}[1]
\FOR {$ k = 1 $ to $ n-m $, $ k \not = j $}
    \STATE $ Q_2[,k] \leftarrow Q_2[,k] - Q_2[i,k] Q_2[,j]/Q_2[i,j] $ \# So $ Q_2[i,k] = 0, \forall k \not = j $ \label{qpivot:0out}
\ENDFOR
\STATE $ Q_2[,j] \leftarrow Q_2[,j]/Q_2[i,j] $ \# So $Q_2[i,j] = 1 $ \label{qpivot:normalize}
\STATE Swap rows $ i $ and $ m+j $ of $ Q_2 $ \label{qpivot:rowswap}
\STATE Swap elements $ i $ and $ m+j $ of $ \pi $ \label{qpivot:permupdate}
\STATE return $ (Q_2, \pi) $
\end{algorithmic}
\end{algorithm}

Notice that the $ (x', Q_2', \pi') $ can be obtained as in section \ref{sec:1pvert}:
Firstly, we swap elements $ i $ and $ m+j $ of $ \pi $ to get $ \pi' $.
Then, we let $ (A_1, A_2) $ be the block decomposition of $ A[,\pi'] $.
So $ x' = \vectwo{A_1^{-1} y}{0} $ and $ Q_2' = \vectwo{-A_1^{-1} A_2}{I_{m-n}} $.

Now we put together these algorithms to get one that navigates from one pseudo-vertex triplet to an adjacent one:

\begin{algorithm}
\caption{Pivot($x$, $Q_2$, $\pi$, $j$)}
\label{algo:pivot}
\begin{algorithmic}[1]
\STATE $ i \leftarrow \mathrm{get\_i}(x, Q_2, j) $
\IF {$ i < 0 $}
    \STATE return ``Cannot move in direction $ Q_2[,j] $''
\ENDIF
\STATE $ x \leftarrow \mathrm{xPivot}(x, Q_2; i, j) $
\STATE $ (Q_2, \pi) \leftarrow \mathrm{QPivot}(Q_2, \pi; i, j) $
\STATE return $ (x, Q_2, \pi) $
\end{algorithmic}
\end{algorithm}

\section{The bounding algorithm}
\label{sec:bdalgo}
We now briefly describe the simplex algorithm, how can it is simplified in determining the set of allowable pivots, and how it gets the generalized Frechet bounds (see the supplementary materiel for a more detailed exposition).
The simplex algorithm splits into two part.
The first takes a pseudo vertex (as in section \ref{sec:1pvert}) as input and outputs a vertex.
This is done by pivoting iteratively from one pseudo vertex triplet $ (x,Q_2,\pi) $ to a next one $ (x', Q'_2, \pi') $ where $ x' $ has less negative coordinates than $ x $ if possible.
If all the neighbors along the direction of $ Q_2 $ have the same number of negative coordinates, we choose one with a maximum negative coordinate (that is, minimum in absolute value) larger than that of $ x $.
One can show this is always possible, which guarantees that a vertex can always be found.

%\begin{algorithm}
%\caption{get\_vert($x$, $Q_2$, $\pi$)}
%\label{algo:getvert}
%\begin{algorithmic}[1]
%\WHILE {x has negative coordinates}
%    \STATE $ i^* \leftarrow \arg\max(x[i]) $ over $ \{i: x[i] < 0\} $ \label{getvert:maxni}
%    \STATE $ J \leftarrow \{j : Q_2[i^*, j] > 0\} $
%    \FOR {$ j \in J $}
%        \STATE try $ (x, Q_2, \pi) \leftarrow \mathrm{Pivot}(x, Q_2, \pi) $
%        \STATE if it succeed get out of the for-loop
%    \ENDFOR
%\ENDWHILE
%\STATE return $ (x,Q_2,\pi) $
%\end{algorithmic}
%\end{algorithm}

The second part takes that first vertex and outputs the vertex optimizing the objective function.
Assume we are at a vertex-triplet $ (x, Q_2, \pi) $, so with the coordinates permuted according to $ \pi $, the objective function is now $ f(x) = x[i] $ for some $ i $.
In contrast with more general linear programs, this function is so simple, that the set of allowable pivots is extremely easy to get.
Recall that we since $ x[m+j] = 0 $ and $ Q_2[m+j,j] = 1 $, we can only add to $ x $ a positive multiples of $ Q_2[,j] $ in order to stay on the polytope (all coordinates must remain non-negative).
The sets of allowable pivots $ (i,j) $ for respectively the maximum and minimum problems are determined respectively by
\begin{eqnarray*}
J_\mathrm{pos} &=& \{j: Q_2[i,j] > 0\} \\
J_\mathrm{neg} &=& \{j: Q_2[i,j] < 0\}
\end{eqnarray*}
If $ J_\mathrm{pos} = \emptyset $, then we cannot increase $ x[i] $ by moving in any direction $ Q_2[,j] $, so we have reached the maximum.
Else we choose a $ j \in J_\mathrm{pos} $, move in direction $ Q_2[,j] $ toward a larger $ x[i] $ and repeat.
Similarly with $ J_\mathrm{neg} = \emptyset $ for the minimum.
Such choices can be made at random or according to well-known rules, such as ``choose the $ j $ that gives the largest increase in $ x[i] $''.

Finally let's mention that some complication can arise when a non-generic (pseudo)-vertex $ x $ (that is, with more than $ n-m $ zero coordinates) is encountered on the way.
We elaborate on a way to get around that in the suplementary materiel.

Now algorithm \ref{algo:getgFb} just uses this version of the simplex algorithm to get all the generalized Frechet bounds.

\begin{algorithm}[h!]
\caption{get\_gF\_bounds($A$, $y$)}
\label{algo:getgFb}
\begin{algorithmic}[1]
\FOR {$ i = 1:n $}
    \STATE Use the simplified simplex algorithm to get $ \min(x[i]) $ and $ \max(x[i]) $ and output them.
\ENDFOR
\end{algorithmic}
\end{algorithm}

\subsubsection{Discussion of the complexity of the algorithm}
The results stated below on the complexity of the simplex method are well known; see for example \cite{Bazaraa2009bk}.
The complexity of algorithm \ref{algo:getgFb} is $ 2n $ times the complexity of the simplified simplex algorithm.
For the complexity of the simplex algorithm, the number of pivots used to achieve the optimum is more relevant than the computation of the allowable pivots at each step.
Indeed, with any known deterministic rule for the choice of a pivot, examples have been built where the simplex algorithm goes through a number of pivots exponential in the size of the problems.
In most ``real life'' problems, though, it goes through a linear number of them.
With a random choice of allowable pivot the average number of pivots is polynomial.
If the problems are restricted to a certain type called \emph{network flow problems}, the number of pivots in the worst case is polynomial.

It turns out that many of the problems we are considering, including contingency problems and network tomography problems that we examine in the next section, are the same problems than some network flow problems.
This is generally the case for problems where the matrix $ A $ is a $ \{0,1\} $ matrix where the sum of the elements in each column equals $ 2 $ (before simplifying to a full rank matrix).
Indeed, the matrix $ A $ can be considered as the incidence matrix of a non-directed network where the rows correspond to nodes and the columns to edges.
The edges have some weights (some ``undirected flow'') and the weight of the nodes is the sum of all the incident edges (the amount of flow each node has to process).
The problem then is to maximize/minimize the weight of a given edge, given a vector of node weights (or capacity).

Therefore it is clear that for these kind of problems even the worst case complexity of algorithm \ref{algo:getgFb} is polynomial and the simplification in computing the set of allowable pivots is relevant.

\section{Experiments}
\label{sec:experim}
The first experiment is set in the same context, of contingency tables, than the traditional Frechet bounds so we can better compare and visualize how much sharper the generalized Frechet bounds are.
The second example is set in the domain of network tomography.
\subsection{Partially filled contingency tables}
Here we generated several random $ 5 $ by $ 5 $ contingency tables $ T $ with a $ \mathrm{Beta}(0.1, 0.1) $ distribution on the entries (this yields more interesting tables than with a uniform distribution).
The tables are vectorized column by column, so the entries are numbered as below:
\[
\begin{array}{|c|c|c|c|c|}
\hline
1 & 6 & 11 & 16 & 21 \\
\hline
2 & 7 & 12 & 17 & 22  \\
\hline
3 & 8 & 13 & 18 & 23  \\
\hline
4 & 9 & 14 & 19 & 24 \\
\hline
5 & 10 & 15 & 20 & 25 \\
\hline
\end{array}
\]
We then computed the marginals and the Frechet bounds $ F_0 = (\min(T_0[1,1], \max(T_0[1,1]) $ for the first entry (first bar in the barplots of fig. \ref{fig:pfillct}).
Then we selected a number of entries to fill, one after the other, and re-computed at each step $ s $ the bounds $ F_s $ for the first entry of the partially filled table $ T_s $.
Notice that if only one entry is unfilled in a row or a column, it can be infered from the the others and the corresponding marginal.
We therefore chose $ 14 $ entries to fill at random but in such a way that at each step there is always at least two entries in each row and each column that are not filled.
We ran this experiment about $ 100 $ times and selected $ 4 $ examples with a more compelling difference between traditional Frechet bounds ($ F_0 $) and generalized bounds ($ F_s $, $ 1 \leq s \leq 14 $).

\begin{figure}[h!]
\begin{center}$
\renewcommand{\arraystretch}{0.6}
\renewcommand{\arraycolsep}{0.6mm}
\begin{array}{cc}
\includegraphics[scale=.25]{ct_22_barplot.pdf} & \includegraphics[scale=.25]{ct_4_barplot.pdf} \\
\begin{array}{|c|c|c|c|c||c}
\hline
 & {\scriptscriptstyle 504} & {\scriptscriptstyle 220} &  & {\scriptscriptstyle 364} & {\scriptscriptstyle 2755} \\
\hline
{\scriptscriptstyle 356} &  &  & {\scriptscriptstyle 1000} & {\scriptscriptstyle 430} & {\scriptscriptstyle 2075} \\
\hline
{\scriptscriptstyle 36} &  &  & {\scriptscriptstyle 72} &  & {\scriptscriptstyle 144} \\
\hline
 & {\scriptscriptstyle 1008} & {\scriptscriptstyle 110} &  & {\scriptscriptstyle 568} & {\scriptscriptstyle 2144} \\
\hline
{\scriptscriptstyle 1231} & {\scriptscriptstyle 252} &  & {\scriptscriptstyle 491} &  & {\scriptscriptstyle 2462} \\
\hline
\hline
{\scriptscriptstyle 3311} & {\scriptscriptstyle 2016} & {\scriptscriptstyle 441} & {\scriptscriptstyle 2000} & {\scriptscriptstyle 1812} & {\scriptscriptstyle 9580}
\end{array}
&
\begin{array}{|c|c|c|c|c||c}
\hline
  & {\scriptscriptstyle 188} & {\scriptscriptstyle 1040} &  & {\scriptscriptstyle 486} & {\scriptscriptstyle 2080} \\
\hline
{\scriptscriptstyle 576} &  & {\scriptscriptstyle 364} & {\scriptscriptstyle 727} &  & {\scriptscriptstyle 2030} \\
\hline
  & {\scriptscriptstyle 438} & {\scriptscriptstyle 110} &  & {\scriptscriptstyle 218} & {\scriptscriptstyle 875} \\
\hline
{\scriptscriptstyle 288} &  &  & {\scriptscriptstyle 980} &  & {\scriptscriptstyle 3224} \\
\hline
{\scriptscriptstyle 1151} & {\scriptscriptstyle 634} &  & {\scriptscriptstyle 426} &  & {\scriptscriptstyle 2636} \\
\hline
\hline
{\scriptscriptstyle 2302} & {\scriptscriptstyle 1707} & {\scriptscriptstyle 2408} & {\scriptscriptstyle 2321} & {\scriptscriptstyle 2107} & {\scriptscriptstyle 10845}
\end{array} \\
\includegraphics[scale=.25]{ct_30_barplot.pdf} & \includegraphics[scale=.25]{ct_31_barplot.pdf} \\
\begin{array}{|c|c|c|c|c||c}
\hline
 & {\scriptscriptstyle 804} & {\scriptscriptstyle 998} & {\scriptscriptstyle 694} &  & {\scriptscriptstyle 3041} \\
\hline
 & {\scriptscriptstyle 100} & {\scriptscriptstyle 50} & {\scriptscriptstyle 26} &  & {\scriptscriptstyle 201} \\
\hline
{\scriptscriptstyle 264} & {\scriptscriptstyle 589} &  &  & {\scriptscriptstyle 162} & {\scriptscriptstyle 1178} \\
\hline
{\scriptscriptstyle 1056} &  &  & {\scriptscriptstyle 1173} & {\scriptscriptstyle 305} & {\scriptscriptstyle 3936} \\
\hline
{\scriptscriptstyle 528} &  &  &  & {\scriptscriptstyle 536} & {\scriptscriptstyle 1601} \\
\hline
\hline
{\scriptscriptstyle 2113} & {\scriptscriptstyle 2296} & {\scriptscriptstyle 1997} & {\scriptscriptstyle 2243} & {\scriptscriptstyle 1308} & {\scriptscriptstyle 9957}
\end{array}
&
\begin{array}{|c|c|c|c|c||c}
\hline
 & {\scriptscriptstyle 1620} &  & {\scriptscriptstyle 553} & {\scriptscriptstyle 236} & {\scriptscriptstyle 3424} \\
\hline
{\scriptscriptstyle 986} &  & {\scriptscriptstyle 80} &  &  & {\scriptscriptstyle 1973} \\
\hline
{\scriptscriptstyle 1552} &  & {\scriptscriptstyle 216} & {\scriptscriptstyle 276} &  & {\scriptscriptstyle 2794} \\
\hline
{\scriptscriptstyle 789} & {\scriptscriptstyle 446} & {\scriptscriptstyle 108} &  &  & {\scriptscriptstyle 1461} \\
\hline
 & {\scriptscriptstyle 726} &  & {\scriptscriptstyle 59} & {\scriptscriptstyle 30} & {\scriptscriptstyle 1452} \\
\hline
\hline
{\scriptscriptstyle 4950} & {\scriptscriptstyle 3239} & {\scriptscriptstyle 433} & {\scriptscriptstyle 1106} & {\scriptscriptstyle 1376} & {\scriptscriptstyle 11104}
\end{array}
\\
\end{array}$
\caption{A selection of $ 4 $ partially filled contingency tables yields the barplots above where the first bar corresponds to the Frechet bounds (unfilled table) for the first entry.
Every subsequent bar corresponds to the generalized bounds for the first entry in the table being cumulatively filled by a new entry (the coordinate number of which is below the bar).
Light grey bars represent the maximum, dark grey ones the minimum.
The final partially filled table is displayed below the barplot.
\label{fig:pfillct}}
\end{center}
\end{figure}

\subsection{Network tomography}
\subsubsection{Basics of network tomography}
In the network tomography problems we consider, networks have nodes of two types:
End-nodes, that we associate to computers, and mid-nodes, that we associate to routers.
The routers are supposed to route the traffic in a deterministic way (the route from one computer to another is determined and stays the same over time).
The traffic at each link (computer-router and router-router) is known (the observed data vector $ y $) and the problem is to infer the traffic between each pair of computer (including a computer to itself via a router).
We assume there are less links than pairs of computers ( a reasonable assumption), so the problem is under-determined.
In our example (see figure \ref{fig:2rout}) the $ 8 $ computers are represented by the letters $ (a,b,c,d,e,f,g,h) $ and the $ 2 $ routers are $ (r1, r2) $.
The $ 18 $ edges are ordered by computer-router first, then router-computer and router-router, lexicographically in each category:
\[
\begin{array}{l}
(a\_r1, b\_r1, c\_r1, d\_r1, \,\,\, e\_r2, f\_r2, g\_r2, h\_r2,\dots \\
\dots r1\_a, r1\_b, r1\_c, r1\_d, \,\,\, r2\_e, r2\_f, r2\_g, r2\_h,\dots \\
\multicolumn{1}{r}{\dots r1\_r2, r2\_r1)}
\end{array}
\]
The $ 64 $ pairs of computer are ordered lexicographically $ (a\_a, a\_b, \dots, h\_g, h\_h) $.
The network matrix $ A $ is the incidence matrix between links (rows) and pairs of computers (columns).
So $ A[i,j] = 1 $ if link $ i $ belongs to the route of the computer pair $ j $, and $ A[i,j] = 0 $ otherwise.

There are dependence relations due to the conservation of flow at each router (the sum of what comes in the router equals the sum of what goes out).
This translates into a rank $ 16 $ for our $ 18 $ by $ 64 $ matrix $ A $.
We can therefore eliminate exactly one link per router and recover its traffic from the flow conservation equation.
Here we deleted rows $ 12 $ and $ 16 $ (links $ r1\_d $ and $ r2\_h $) of the matrix and of the vector $ y $.
The resulting $ 16 $ by $ 64 $ matrix has now full rank.

We borrowed our example and the data from \cite{Cao2000pap}.
They gathered link traffic on a $2$-router network over $ 288 $ time slots spaced out by a period of $ 5 $ minutes.
Unfortunately their data is incomplete since there is no record of traffic on the links $ r1_r2 $ and $ r2_r1 $ (and indeed the $ 16 $ by $ 64 $ matrix resulting from removing the corresponding rows from the incidence matrix has rank $ 15 $).

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.6]{netw_2rout.pdf}}
\caption{An example of $2$-router network with link traffic (at time $47$).
What are the bounds for the traffic between each pair of computers?
\label{fig:2rout}}
\end{center}
\vskip -0.2in
\end{figure}

To remedy this problem we simulated traffic for this missing data (see figure \ref{fig:2rout}).
The traffic on these two links has a maximum and a minimum.
Let $ S_{out} $ and $ S_{in} $ be respectively the sum of the traffic on the links from $ (a,b,c,d) $ to $ r\_1 $ and from $ r\_1 $ to $ (a,b,c,d) $, and similarly $ T_{out} $ and $ T_{in} $ the sum of the traffic on the links from $ (e,f,g,h) $ to $ r\_2 $ and from $ r\_2 $ to $ (e,f,g,h) $.
Then
\[
\max(0, S_{out} - S_{in}) \leq \mathrm{traffic}(r1\_r2) \leq \min(S_{out}, T_{in})
\]
And similarly with $ \mathrm{traffic}(r2\_r1) $.
So we simulated these two traffics by arbitrarily taking the (integer) midpoint of these intervals.
See the whole link traffic at time point $ 47 $ on figure \ref{fig:2rout}.
Next we eliminated rows $ 12 $ and $ 16 $ of the incidence matrix and of the vector $ y $, as mentioned above.
Now we are in the situation of equation \ref{eq:Axy_xnn} with a full rank $ 16 $ by $ 64 $ matrix $ A $ and a $ 16 $-dimensional vector $ y $.

\subsubsection{Minimum bound Data analysis}

Typical questions of interest are "Is there any pair of computer traffic that stands out from the rest? How much so, when and how often?".
This is a broad question, but in our context minimum bounds that are positive tend to be relatively rare (theorem \ref{thm:nnztmin}), and are a natural feature for spotting such notable traffic.
We computed the minimum bounds for all the $ 64 $ coordinates at each of the $ 288 $ time points.
Coordinate $ 10 $, corresponding to the pair $ b\_b $ had a positive minimum $ 43 $ times out of $ 288 $.
The next coordinate with most frequent positive minimum ($ 9 $ out of $ 288 $) is coordinate $ 26 $, correpsonding to the pair $ d\_b $.
So the pair $ b\_b $ has a traffic that stands out most often.
The time point at which there are the greatest number of positive minimums is time point $ 47 $ with $ 4 $ positive minimums for pairs $ b\_a, b\_b, b\_c, b\_d $ (see figure \ref{fig:2rout}).
Let's look more closely at that time point.
The bounds for the $ 4 $ pairs are
\[
\begin{array}{c||c|c|c|c|}
& \mathrm{b\_a} & \mathrm{b\_b} & \mathrm{b\_c} & \mathrm{b\_d} \\
\hline\hline
\min & 89094 & 437242 & 314699 & 22709 \\
\hline
\max & 198720 & 546868 & 424325 & 132334 \\
\hline
\end{array}
\]










\bibliography{../bibtex/gen_frech}
\bibliographystyle{plain}

\end{document}
