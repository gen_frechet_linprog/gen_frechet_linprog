\section{Introduction}
\label{sec:intro}
Cross-classified categorical data is analyzed in tables called \emph{contingency tables} (see for example \cite{Fienberg2007bk}).
Analyzing the dependence of such data usually requires to compute marginals of this table like sums of rows and sums of columns for $2$-way tables.
For multiway tables ($k$-way with $ k \geq 2 $), the marginals to compute are determined by the hypothesis of independence (see for example \cite{Dobra2000pap}).
Then one typically compute the expected table with such marginals and the deviance of the given table from the expected table.
Precise calculations require some assumption or inference on the distribution of tables with those given marginals.
In this process it is helpful to have some tight bounds on the entries of the table.
Such bounds, called Frechet bounds, are well-known and are used as well in other contexts, like the theory of copulas (see for example \cite{Joe1997bk}).
In the simpler case of a $2$-way contingency table $(x)_{ij}$, $i = 1,\dots, I $, $ j = 1, \dots, J $, the marginals are $ x_{i+} = \sum_j x_{ij} $ and $ x_{+j} = \sum_i x_{ij} $, and with total count $ N = \sum_{ij} x_{ij} $, the bounds are
\[
\max(0, x_{i+} + x_{+j} - N) \leq x_{ij} \leq \min(x_{i+}, x_{+j})
\]
From this formula a natural question arises:
How many entries have a positive minimum bound.

Beside being very easy to compute, Frechet bounds are sharp.
That is, for every $ i,j $, there is a table where $ x_{ij} $ is equal to the lower bound and there is a table where $ x_{ij} $ is equal to the upper bound.
It turns out that the set of all tables with some marginals fixed is a polytope $ P $ of dimension $ (I-1)*(J-1) $ (see section \ref{}).
So from a geometric perspective, Frechet bounds define a tight bounding box of dimension $ I*J $ that contains $ P $ as illustrated in figure \ref{fig:bound_box}.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.6]{bound_box.pdf}}
\caption{Frechet bounds define a tight bounding box for the solution polytope $ \mathcal{P} $.
Projecting on coordinate spaces preserves the bounds.
Notice the top figure is already a projection from dimension $ n \geq 5 $ to dimension $ 3 $ ($ \mathcal{P} $ has $ 5 $ facets).
\label{fig:bound_box}}
\end{center}
\vskip -0.2in
\end{figure} 

However, if the table contains structural zeros, that is, if it is known that some entries are always $ 0 $, or equivalently if the table is partially filled, Frechet bounds fail to be sharp.
All this generalizes easily (at least in concept) to multi-way tables.

Contingency tables with given marginals are solutions of an under-determined linear system
\begin{equation*}
\label{init_syst}
Ax = y \qquad x \in \mathbb{N}^n
\end{equation*}
where $ x $ is the vectorized list of table entries, $y$ is the vectorized list of marginal entries, and $ A $ is the incidence matrix between the table entries and the marginals.
That is, $ A[i,j] = 1 $ if $ x[j] $ contributes to the marginal entry $ y[i] $, and $ A[i,j] = 0 $ otherwise.
Since some marginal entries can be recovered from the rest, some rows can be eliminated so that $ A $ is a full rank $m$ by $n$ matrix.
Many other problems can be cast in terms of such a system.
They form an linear sub-class of a larger class called \emph{ill-posed inverse problems}.
They are inverse problems because one tries to infer information about the object (here the table) from observations (marginal counts).
They are ill-posed because there are usually more than one solution (or no solution at all,e.i. the problem is not \emph{feasible}).
Such problems include contingency tables with given marginals, including tables with structural zeros (see \cite{Chen2007pap}), network tomography (see \cite{Vardi1996pap}, \cite{Castro2004pap}), mass transportation problems (see \cite{Rachev1999pap}), networks with given degree vectors (see \cite{Blitzstein2011pap}, \cite{Chatterjee2010pap}), and flux-balance analysis (see \cite{Orth2010prim}).
All these problems have a solution space that is a polytope (usually in high dimension).
However, no efficient algorithm has been proposed (to the authors' knowledge) to compute Frechet-like sharp bounds for such general systems.

After some background on the geometry and the linear algebra of the solution space (sections \ref{sec:geomsol} and \ref{sec:linalg}), we present in section \ref{sec:bdalgo} such an algorithm.
The number of coordinates of $ x $ that have positive minimum bound is always of interest and in sections \ref{ssec:thm1proof} we prove the following theorem:
\begin{thm}
\label{thm:nnztmin}
The maximum number of coordinates of $ x $ with a positive minimum bound is $ m - 1 $.
\end{thm}
In section \ref{} we show how our algorithm translates into a specialized simplex method.
We take advantage of that correspondance to show that the average complexity of computing generalized Frechet bound is polynomial in the size of the problem.
Finally section \ref{} is devoted to an example.


