\documentclass{article}

\usepackage{amsthm,amsmath,amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{afterpage}
\usepackage{lgreek}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{define}[thm]{Definition}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}

\DeclareMathOperator{\interior}{interior}

\graphicspath{{../figures/}}

\newcommand{\vectwo}[2]{
    \left(\begin{array}{c}
     {#1} \\ {#2}
    \end{array}\right)
}
\newcommand{\sign}{\operatorname{sign}}
\newcommand{\inv}{\operatorname{inv}}

\title{Generalized Frechet bounds and applications}
\author{Bertrand Haas\\
    Dept. of Statistics, Harvard University}

\begin{document}
\maketitle

\begin{abstract}
\input{abstract.tex}
\end{abstract}

\input{intro.tex}

\section{Polyhedral Geometry}
\label{sec:geomsol}
We describe here the polyhedral geometry of the solution set $ P $ to \ref{eq:Axy_xnn} and we use this to prove proposition \ref{prop:nnocutbd}.

Since it is linear, it is clear that $ P $ is convex:
If $ x_1 $ and $ x_2 $ are solutions, then $ t x_1 + (1-t) x_2 $ is again a solution for any $ 0 < t < 1 $.
Without loss of generality we can assume that the coordinate to bound is the first one, $ x[1] $.
Let $ H(t) = \{x \in \mathcal{A}: x[1] = t\} $ and let $ t $ vary continuously from $ 0 $ on.
Since $ P $ is convex, the hyperplane $ H(t) $ will intersect $ P $ exactly for $ t $ in a range $ t_\mathrm{min} \leq t \leq t_\mathrm{max} $.

The solution space $ \mathcal{A} $ to $ Ax = y $ is the intersection of $ m $ independent ($ A $ has full rank) hyperplanes defined by the rows $ A[i,] x = y[i] $.
So it has dimension $ d = n-m $.
Now $ \mathcal{A} $ intersects the non-negative orthant into a polyhedron $ P $.
If $ \mathcal{A} $ intersects the interior of the non-negative orthant, then $ \dim(P) = d = n - m $.
If $ \mathcal{A} $ intersects the non-negative orthant only on its boundary, then $ \dim(P) < n-m $.
Finally if $ \mathcal{A} $ does not intersect the non-negative orthant, then $ P = \emptiset $ and the system \ref{eq:Axy_xnn} is said to be \emph{infeasible}.

%For simplicity of exposition we assume here that this intersection is bounded (so it is a polytope $ \mathcal{P} $) and full dimensional (so $ \dim(P) = n-m $).

The affine space $ \mathcal{A} $ intersects also the hyperplanes of coordinates $ \{x: x[i] = 0\} $ into hyperplanes $ H_i \subset \mathcal{A} $.
This defines an \emph{arrangement of hyperplanes} $ \mathcal{H} = \{H_i, i = 1, \dots, n \} $ that delimit polyhedral cells.
These cells are the intersections of $ \mathcal{A} $ with orthants and can therefore be labeled by an $n$-sign vector.
In particular the intersection with the non-negative orthant is $ \mathcal{P} $ and is labeled with $ n $ plusses $ (+,+, \dots, +) $ (see figure \ref{fig:bdhyp5}).
So $ P $ can also be described as the intersection $ \cap H_i^+ $, $ i = 1, \dots, n $, of half spaces $ H_i^+ = \{x \in \mathcal{A}: x[i] \geq 0\} $.

The vertices of the hyperplane arrangement are the intersections of $ \mathcal{A} $ with the coordinate planes of dimension $ m $.
Indeed, the dimension of such intersection is $ (n-m) + (m) - n = 0 $.
Therefore $ \mathcal{H} $-vertexes have at most $ m $ non-zero coordinates.
And reciprocally any solution with at least $ n-m $ zeros coordinates is an $ \mathcal{H} $-vertex.
Vertexes of the solution polyhedron, or $ P $-vertexes, are $ \mathcal{H} $-vertexes with all their non-zero coordinates positives.
And reciprocally any $ \mathcal{H} $-vertex with all its non-zero coordinatesa positive is a $ P $-vertex.
%We call \emph{pseudo-vertex} a point of $ \mathcal{A} $ with at most $ m $ non-zero coordinates (positive or negative).

\subsection{Infinitesimal Pulling and Pushing of Half-Spaces}
We can modify the arrangement of hyperplanes $ \mathcal{H} $ by pulling away some $ H_i $.

\begin{define}
Let $ H_i(a) = \{x \in \mathcal{A}: x[i] = a \} $ for some $ a $ and $ H_i^+(a) = \{x \in \mathcal{A} : x[i] \geq a\} $ the corresponding half-space.
We say $ H_i(a) $ (respectively $ H_i^+(a) $) is \emph{pulled} from $ H_i $ (respectively $ H_i^+ $) if $ a < 0 $ and \emph{pushed} if $ a > 0 $.
\end{define}

$ P $ has lower dimension than $ n-m $, if and only if it is contained in a hyperplane $ H_{ij} = \{x : x[i] = - x[j] \} $ (so it is contained in $ H_i = H_j = H_i^+ \cap H_j^+ $).
Therefore by slightly pushing $ H_i $ the polyhedron vanishes, that is, the new system becomes infeasible.
We use this property for the following definition.

\begin{define}
$ P $ is \emph{dimension deficient} if for all $ \epsilon > 0 $ small enough, $ P \cap H_i^+(\epsilon) = \emptyset $.
\end{define}

A hyperplane $ H_i $ might not be supporting a facet of $ P $.
Even if it does support a face of lower dimension, pulling $ H_i $ will not affect $ P $ itself.
We use this property for the following definition.

\begin{define}
A hyperplane $ H_i $ is \emph{redundant} if for all $ \epsilon > 0 $ small enough, $ \cap_{j \not = i} H_j^+ \cap H_i^+(-\epsilon) = P $.
\end{define}

\section{Useless coordinates and minimum bounds}
\label{sec:nocutbd}
If all we are interested in is the solution polyhedron $ P $, for example for sampling points from $ P $, optimising a function on $ P $ or computing the volume of $ P $, anything that does not contribute to its structure can be deemed ``useless''.
In particular:

\begin{define}
In the system \ref{eq:Axy_xnn}, a coordinate $ x[i] $ is said to be \emph{useless} if the corresponding hyperplane $ H_i $ is redundant. \\
We will also use the term \emph{useless} as a synonym to \emph{redundant} for $ H_i $.
\end{define}

So useless coordinates can be removed and we will see in section \ref{sec:??:} how to do that.
Before removing them, though, it is essential to detect them.
Clearly, having a positive minimum if a sufficient condition for $ x[i] $ to be useless (it means that $ H_i $ does not intersect $ P $).
But $ x[i] $ can still be useless with a zero minimum.
In that case $ H_i $ supports a face of $ P $ of lower dimension than a facet.
But by pulling $ H_i $ to $ H_i(\epsilon) $ we get a polyhedron $ P' $ on which the minimum of $ x[i] $ is of the order of $ \epsilon > 0 $.

Finding the minimum of $ x[1] $ is easily done by the simplex method.
The simplex method is a well-known algorithm that finds the minimum of a linear function $ c(x) $ over a polyhedron defined by a system like \ref{eq:Axy_xnn}.
This algorithm is well-known to splits naturally into two routines.
The first routine $ \mathrm{get\_first\_vert}(A, b) $, finds a first $ P $-vertex $ v_0 $.
It does so by starting at a first $ H $-vertex $ w_0 $ and by moving from $ H $-vertex $ w_i $ to $ H $-vertex $ w_{i+1} $ closer to a $ P $-vertex.
The second routine $ \mathrm{get_min_from}(v_0, A, b, c) $ finds a $ P $-vertex where the minimum of $ c(x) $ is attained.
It does so by moving from $ P $-vertex $ v_i $ to $ P $-vertex $ v_{i+1} $ where $ c(v_{i+1}) < c(v_i) $.

Most implementation, though, are numerical implementations.
To deal with infinitesimals we can either use a small numerical value for $ \epsilon $ (but then we have to be very careful in the computation of numerical errors), or we can implement a symbolic version (with $ \epsilon $ the symbol for an \emph{infinitesimal} value) of the simplex method.
In the latter case we need to implement the arithmetic of infinitesimals:
\begin{eqnarray*}
(a + b\epsilon) + (c + d\epsilon) &=& (a+c) + (b+d)\epsilon \\
(a + b \epsilon) (c + d\epsilon) &=& (ac) + (ad + bc)\epsilon
\end{eqnarray*}
And the order relation
\[
(a + b\epsilon) > (c + d\epsilon)
\]
if and only if $ a > c $ or $ a = c $ and $ b > d $.
With such an implementation of $ \mathrm{get\_first\_vert}(A,b) $ and $ \mathrm{get_min\_from}(v_0, A, b, c) $ we can state the following.

\begin{proposition}
Algorithm \ref{algo:detect_i} finds whether a coordinate $ x[i] $ is useless or not.
\end{proposition}

\begin{algorithm}
\caption{detect\_useless($A$, $b$, $i$; $v_0$)}
\label{algo:detect_i}
\begin{algorithmic}[1]
\STATE Let $ b' = b - \epsilon A[,i] $ \label{detect_i:bprime}
\STATE Let $ v_1 = \mathrm{get\_min\_from}(v_0, A, b', x[i]) $
\IF ($ v_1[i] > 0 $)
    \STATE return ($v_1$, "useless")
\ELSE
    \STATE return ($v_1$, "not useless")
\ENDIF
\end{algorithmic}
\end{algorithm}

\begin{proof}
Algorithm \ref{??} will get a first $ P $-vertex $ v_0 $ before using this agorithm.
This is why the input has $ v_0 $.

Let $ P' $ be the new polyhedron obtained by pulling $ H_i $ to $ H_i' = \{x \in \mathcal{A}: x[i] = -\epsilon\} $.
So the $ i^\mathrm{th} $ coordonate changes to $ x'[i] = x[i] + \epsilon $.
Therefore the defining equation \ref{eq:Axy_xnn} becomes:
\begin{eqnarray}
A x &=& b \\
A (x' - \mathbf{\epsilon}) &=& b \\
A x' &=& b + \epsilon A[,i]
\end{eqnarray}
Where $ \mathbf{\epsilon} $ is the vector with $ \epsilon $ at coordinate $ i $ and zero elsewhere.
This explains line \ref{detect_i:bprime}

Now that we pulled $ H_i $, we check whether it is redundant by checking if the minimum for $ x[i] $ is positive (inifinitesimally, or not).
\end{proof}

















\section{The Geometry of the Simplex Method}
Let $ v_0 $ be a vertex of $ P $.
As we saw in section \ref{sec:geomsol}, $ v_0 $ has at least $ d = n-m $ zero coordinates.
If $ v_0 $ has exactly $ d $ zero coordinates, then it is a \emph{simple} vertex, that is, it is the intersection of exactly $ d $ coordinate hyperplanes $ H_i $.
In that case every such $ H_i $ supports a facet of $ P $.

Complication arises when $ v_0 $ has $ d+k $ zero coordinates, $ k > 0 $, that is, when it is not a simple vertex.
A coordinate hyperplane $ H_i $ might be redundant (that is, coordinate $ i $ is useless), or it might not, and more work is required to figure this out.
In order to do so we need to delve a little more into the geometry of the simplex method and show how we get, together with the first vertex

For simplicity of exposition, let's permute the coordinates so that the last $ d+k $ coordinates of $ v_0 $ are the zero ones ($ k \geq 0 $).
With the columns of $ A $ permuted that way, we let $ A_1 = A[,1:m] $ and $ A_2 = A[,(m+1):n] $, let $ Q_{12} = -A_1^{-1} A_2 $ and $ Q_2 = \vectwo{Q_{12}}{I_d} $ with the $ d $-dimensional identity matrix as lower block.

\begin{prop}
\label{prop:Q2}
\begin{enumerate}
\item \label{prop:Q2_basis} The columns of $ Q_2 $ form a basis of the null space of $ A $.
\item \label{prop:Q2_intersect} The direction spanned by $ Q_2[,i] $ is the intersection $ \cap H_j $ for $ j = m+1, \dots, n $, $ j \not = m+i $.
\item The cone $ \mathcal{C} = \{x: x = v_0 + Q_2 \lambda, \lambda \geq 0\} $ contains $ P $.
\end{enumerate}
\end{prop}

\begin{proof}
\begin{enumerate}
\item By construction we have $ A Q_2 = 0 $.
    Moreover since the lower block $ Q_2[(m+1):n,] $ is the $d$-dimensional identity matrix, the $ d $ columns of $ Q_2 $ are independent.
    Therefore the columns of $ Q_2 $ form a basis of the null space of $ A $.
\item By construction $ Q_2[j,i] = 0 $ for $ j > m $, $ j \not = m+i $ and $ Q_2[m+i,i] = 1 $.
    Therefore $ Q_2[,i] \in H_i $ for $ j > m $, $ j \not = m+i $.
    Moreover, since $ v_0 = \cap H_i $, $ i = m+1, \dots, n $, and $ P $ is full dimensional, the $ H_i $ are linearly independent.
    So the intersection $ \cap H_i $, for $ i = m+1, \dots, n $, $ j \not = m+i $, is one dimensional.
    Therefore it is the direction spanned by $ Q[,i] $.
\item From \ref{prop:Q2_basis} we know that any point $ x \in P $ can be written $ x = v_0 + Q_2 \lambda $.
    Since $ Q_2[(m+1):n,] = I_d $ and the coordinates of $ x $ are non-negative, this implies that $ \lambda \geq 0 $, in other words $ x \in \mathcal{C} $.
\end{enumerate}
\end{proof}

Recovering directions to neighbors:
If $ Q_2[j,i] \geq 0 $ for $ 1 \leq j \leq m $ such that $ v_0[j] = 0 $, then $ v^{(t)} = v_0 + t Q_2[,j] $ will still have all its coordinates non-negative for $ t > 0 $ small enough.
That is, $ v^{(t)} \in P $ and since it has at most $ m+1 $ non-zero coordinates (the ones from $ v_0 $ plus the $ m+j $ coordinate equal to $ t $), $ v^{(t)} $ travels on the edge of $ P $ defined by $ Q_2[,j] $.

Pivoting:
The first $ t = t_1 > 0 $ for which $ v^{(t)}[k] = 0 $ defines a neighbor vertex $ v^{(t_1)} = v_1 $ to $ v_0 $ since it has at most $ m $ non-zero coordinates.
This happens when $ v_t $ meets $ H_k $.
So $ v^{(t)} $ leaves $ H_i $ at $ t = 0 $, travels along $ Q_2[,j] = \cap_\ell H_\ell $ for $ \ell = m+1, \dots, n $, $ \ell \not = i $ (the remaining $ H_\ell $), and stops at $ v_1 $ when it meets $ H_k $.
At this point $ v_0 $ is updated to $ v_1 = v_0 + t_1 Q_2[,j] $ with $ v_1[k] = 0 $ and $ v_1[m+i] = t_1 $.
n order to keep the same presentation than for $ v_0 $, that is the last $ d $ coordinates being zero, we swap coordinates $ k $ and $ m+i $, which yields a new permutation of the coordinates.
In order to keep the same block presentation for $ Q_2 $ we need to update it as well.
We could do as before and redefine $ A = (A_1, A_2) $ with the new permutation of $ A $'s columns, and re-compute $ Q_{12} = -A_1^{-1} A_2 $ and $ Q_2 = \vectwo{Q_{12}}{I_d} $.
But since this is a rank-one update, the simplex algorithm provides a more economical way of doing so.
We will just skip it here since this is not essential. (OR WRITE IT IN THE APPENDIX)

Virtual pivoting:
If $ Q_2[j,i] < 0 $ for some $ 1 \leq j \leq m $ such that $ v_0[j] = 0 $, then for any $ t > 0 $, the point $ v_t = v+0 + t Q_2[,j] $ will have at least its $ i $ coordinate negative.
Therefore it is not on $ P $ anymore and $ Q_2[,j] $ does not point toward a neighbor $ P $-vertex of $ v_0 $.
However, we can still do some pivoting of $ Q_2 $ to change basis in the null space of $ A $.
This amount to switching the coordinates $ v_0[j] $ with $ v_0[m+i] $, which are both zero.
In other words we virtually move from $ v_0 $ to $ v_1 = v_0 $.
For $ Q_2 $, this amounts to changing each column $ Q_2[,k] $, $ k \not = i $ from $ \cap_\ell H_\ell $, $ \ell = m+1, \dots, n $, $ \ell \not = k $, to $ Q'_2[,k] = \cap_\ell H_\ell \cap H_$, $ \ell = m+1, \dots, n $, $ \ell \not = k, m+i $.
We call this operation \emph{virtual pivoting}
Therefore by virtual pivoting

If $ k > 0 $ The directions given by 




MORE






Detecting no-cut minimum bounds amounts to getting the dimensions of the intersections of the hyperplanes $ H_i $ and $ \mathcal{P} $.
This is done by algorithm \ref{algo:getncutbd} as anounced by proposition \ref{prop:getncutbd} which we prove now:
\begin{proof}

\end{proof}

\begin{algorithm}
\caption{get\_nocut($A$, $y$, $k$) \# $ A $ is $ m $ by $ n $}
\label{algo:getncutbd}
\begin{algorithmic}[1]
\STATE Run part I of the simplex algorithm on the system $ A[,-k] x = y $
\IF {The output is ``system infeasible''}
    \STATE return ``Minimum Frechet bound is positive''
\ELSE {\# The output is a vertex of $ \mathcal{P} $}
    \STATE Recover directions to neighbors
    \IF {directions are independent}
        \STATE return ``0 normal minimum Frechet bound''
    \ELSE
        \STATE return ``0 no-cut minimum Frechet bound''
    \ENDIF
\ENDIF
\end{algorthmic}
\end{algorithm}







algorithm \ref{algo:getncutbd}:









\section{The bounding algorithm}
\label{sec:bdalgo}
We now briefly describe the simplex algorithm, how can it is simplified in determining the set of allowable pivots, and how it gets the generalized Fr\'echet bounds (see the supplementary materiel for a more detailed exposition).
The simplex algorithm splits into two part.
The first takes a pseudo vertex (as in section \ref{sec:linalg}) as input and outputs a vertex.
This is done by pivoting iteratively from one pseudo vertex triplet $ (x,Q_2,\pi) $ to a next one $ (x', Q'_2, \pi') $ where $ x' $ has less negative coordinates than $ x $ if possible.
If all the neighbors along the direction of $ Q_2 $ have the same number of negative coordinates, we choose one with a maximum negative coordinate (that is, minimum in absolute value) larger than that of $ x $.
One can show this is always possible, which guarantees that a vertex can always be found.

%\begin{algorithm}
%\caption{get\_vert($x$, $Q_2$, $\pi$)}
%\label{algo:getvert}
%\begin{algorithmic}[1]
%\WHILE {x has negative coordinates}
%    \STATE $ i^* \leftarrow \arg\max(x[i]) $ over $ \{i: x[i] < 0\} $ \label{getvert:maxni}
%    \STATE $ J \leftarrow \{j : Q_2[i^*, j] > 0\} $
%    \FOR {$ j \in J $}
%        \STATE try $ (x, Q_2, \pi) \leftarrow \mathrm{Pivot}(x, Q_2, \pi) $
%        \STATE if it succeed get out of the for-loop
%    \ENDFOR
%\ENDWHILE
%\STATE return $ (x,Q_2,\pi) $
%\end{algorithmic}
%\end{algorithm}

The second part takes that first vertex and outputs the vertex optimizing the objective function.
Assume we are at a vertex-triplet $ (x, Q_2, \pi) $, so with the coordinates permuted according to $ \pi $, the objective function is now $ f(x) = x[i] $ for some $ i $.
In contrast with more general linear programs, this function is so simple, that the set of allowable pivots is extremely easy to get.
This allows us to simplify this step in the usual simplex method:
Recall that since $ x[m+j] = 0 $ and $ Q_2[m+j,j] = 1 $, we can only add to $ x $ a positive multiples of $ Q_2[,j] $ in order to stay on the polytope (all coordinates must remain non-negative).
The sets of allowable pivots $ (i,j) $ for the maximum and minimum problems are determined respectively by
\begin{eqnarray*}
J_\mathrm{pos} &=& \{j: Q_2[i,j] > 0\} \\
J_\mathrm{neg} &=& \{j: Q_2[i,j] < 0\}
\end{eqnarray*}
If $ J_\mathrm{pos} = \emptyset $, then we cannot increase $ x[i] $ by moving in any direction $ Q_2[,j] $, so we have reached the maximum.
Else we choose a $ j \in J_\mathrm{pos} $, move in direction $ Q_2[,j] $ toward a larger $ x[i] $ and repeat.
Similarly with $ J_\mathrm{neg} $ for the minimum.
The choice for a $ j \in J_\mathrm{pos} $ can be made at random or according to well-known rules, such as ``choose the $ j $ that gives the largest increase in $ x[i] $''.

Finally let's mention that some complication can arise when a non-generic (pseudo)-vertex $ x $ (that is, with more than $ n-m $ zero coordinates) is encountered on the way.
We elaborate on a way to get around that in the suplementary materiel.

Now algorithm \ref{algo:getgFb} just uses this version of the simplex algorithm to get all the generalized Fr\'echet bounds.

\begin{algorithm}[h!]
\caption{get\_gF\_bounds($A$, $y$)}
\label{algo:getgFb}
\begin{algorithmic}[1]
\FOR {$ i = 1:n $}
    \STATE Use the simplified simplex algorithm to get $ \min(x[i]) $ and $ \max(x[i]) $ and output them.
\ENDFOR
\end{algorithmic}
\end{algorithm}

\subsection{Discussion of the complexity of the algorithm}
The results stated below on the complexity of the simplex method are well known; see for example \cite{Bazaraa2009bk}.
Since we run our version of the simplex algorithm at most twice (for minimum, if the bound from proposition \ref{prop:nnocutbd} is not attained, and for maximum) over each coordinate, the complexity of algorithm \ref{algo:getgFb} is $ 2n $ times the complexity of the simplified simplex algorithm.
For the complexity of the simplex algorithm, the number of pivots used to achieve the optimum is more relevant than the computation of the allowable pivots at each step.
Indeed, with any known deterministic rule for the choice of a pivot, examples have been built where the simplex algorithm goes through a number of pivots that is exponential in the size of the problems.
In most ``real life'' problems, though, it goes through a linear number of them.
Moreover, if at each pivoting step we choose the pivot at random in the set of allowable pivots, the average number of pivoting steps is polynomial and our simplification is again relevant.
If the problems are restricted to a certain type called \emph{network flow problems}, the number of pivots in the worst case is polynomial.

It turns out that many of the problems we are considering, including contingency problems and network tomography problems that we examine in the next section, are the isomorphic to network flow problems.
This is generally the case for problems where the matrix $ A $ is a $ \{0,1\} $ matrix where the sum of the elements in each column equals $ 2 $ (before simplifying to a full rank matrix).
Indeed, the matrix $ A $ can be considered as the incidence matrix of a non-directed network where the rows correspond to nodes and the columns to edges.
The edges have some weights (some ``undirected flow'') and the weight of the nodes is the sum of all the incident edges (the amount of flow each node has to process).
The problem then is to maximize/minimize the weight of a given edge, given a vector of node weights (or capacity).

Therefore it is clear that for these kind of problems even the worst case complexity of algorithm \ref{algo:getgFb} is polynomial and our simplification is again relevant.

\section{Experiments}
\label{sec:experim}
The first experiment is set in the same context, of contingency tables, as the traditional Fr\'echet bounds so we can better compare and visualize how much sharper the generalized Fr\'echet bounds are.
The second example is set in the domain of network tomography.
\subsection{Partially filled contingency tables}
Here we generated several random $ 5 $ by $ 5 $ contingency tables $ T $ with a $ \mathrm{Beta}(0.1, 0.1) $ distribution (scaled and rounded off) on the entries (this yields more interesting tables than with a uniform distribution).
The tables are vectorized column by column, so the entries are numbered as below:
\[
\begin{array}{|c|c|c|c|c|}
\hline
1 & 6 & 11 & 16 & 21 \\
\hline
2 & 7 & 12 & 17 & 22  \\
\hline
3 & 8 & 13 & 18 & 23  \\
\hline
4 & 9 & 14 & 19 & 24 \\
\hline
5 & 10 & 15 & 20 & 25 \\
\hline
\end{array}
\]
We then computed the marginals and the Fr\'echet bounds $ F_0 = (\min(T_0[1,1], \max(T_0[1,1]) $ for the first entry (first bar in the barplots of fig. \ref{fig:pfillct}).
Then we selected a number of entries to fill, one after the other, and re-computed at each step $ s $ the bounds $ F_s $ for the first entry of the partially filled table $ T_s $.
Notice that if only one entry is unfilled in a row or a column, it can be infered from the the others and the corresponding marginal.
We therefore chose $ 14 $ entries to fill at random but in such a way that at each step there is always at least two entries in each row and each column that are not filled.
We ran this experiment about $ 100 $ times and selected $ 4 $ examples with a more compelling difference between traditional Fr\'echet bounds ($ F_0 $) and generalized bounds ($ F_s $, $ 1 \leq s \leq 14 $).

\begin{figure}[h!]
\begin{center}$
\renewcommand{\arraystretch}{0.6}
\renewcommand{\arraycolsep}{0.6mm}
\begin{array}{cc}
\includegraphics[scale=.25]{ct_22_barplot.pdf} & \includegraphics[scale=.25]{ct_4_barplot.pdf} \\
\begin{array}{|c|c|c|c|c||c}
\hline
 & {\scriptscriptstyle 504} & {\scriptscriptstyle 220} &  & {\scriptscriptstyle 364} & {\scriptscriptstyle 2755} \\
\hline
{\scriptscriptstyle 356} &  &  & {\scriptscriptstyle 1000} & {\scriptscriptstyle 430} & {\scriptscriptstyle 2075} \\
\hline
{\scriptscriptstyle 36} &  &  & {\scriptscriptstyle 72} &  & {\scriptscriptstyle 144} \\
\hline
 & {\scriptscriptstyle 1008} & {\scriptscriptstyle 110} &  & {\scriptscriptstyle 568} & {\scriptscriptstyle 2144} \\
\hline
{\scriptscriptstyle 1231} & {\scriptscriptstyle 252} &  & {\scriptscriptstyle 491} &  & {\scriptscriptstyle 2462} \\
\hline
\hline
{\scriptscriptstyle 3311} & {\scriptscriptstyle 2016} & {\scriptscriptstyle 441} & {\scriptscriptstyle 2000} & {\scriptscriptstyle 1812} & {\scriptscriptstyle 9580}
\end{array}
&
\begin{array}{|c|c|c|c|c||c}
\hline
  & {\scriptscriptstyle 188} & {\scriptscriptstyle 1040} &  & {\scriptscriptstyle 486} & {\scriptscriptstyle 2080} \\
\hline
{\scriptscriptstyle 576} &  & {\scriptscriptstyle 364} & {\scriptscriptstyle 727} &  & {\scriptscriptstyle 2030} \\
\hline
  & {\scriptscriptstyle 438} & {\scriptscriptstyle 110} &  & {\scriptscriptstyle 218} & {\scriptscriptstyle 875} \\
\hline
{\scriptscriptstyle 288} &  &  & {\scriptscriptstyle 980} &  & {\scriptscriptstyle 3224} \\
\hline
{\scriptscriptstyle 1151} & {\scriptscriptstyle 634} &  & {\scriptscriptstyle 426} &  & {\scriptscriptstyle 2636} \\
\hline
\hline
{\scriptscriptstyle 2302} & {\scriptscriptstyle 1707} & {\scriptscriptstyle 2408} & {\scriptscriptstyle 2321} & {\scriptscriptstyle 2107} & {\scriptscriptstyle 10845}
\end{array} \\
\includegraphics[scale=.25]{ct_30_barplot.pdf} & \includegraphics[scale=.25]{ct_31_barplot.pdf} \\
\begin{array}{|c|c|c|c|c||c}
\hline
 & {\scriptscriptstyle 804} & {\scriptscriptstyle 998} & {\scriptscriptstyle 694} &  & {\scriptscriptstyle 3041} \\
\hline
 & {\scriptscriptstyle 100} & {\scriptscriptstyle 50} & {\scriptscriptstyle 26} &  & {\scriptscriptstyle 201} \\
\hline
{\scriptscriptstyle 264} & {\scriptscriptstyle 589} &  &  & {\scriptscriptstyle 162} & {\scriptscriptstyle 1178} \\
\hline
{\scriptscriptstyle 1056} &  &  & {\scriptscriptstyle 1173} & {\scriptscriptstyle 305} & {\scriptscriptstyle 3936} \\
\hline
{\scriptscriptstyle 528} &  &  &  & {\scriptscriptstyle 536} & {\scriptscriptstyle 1601} \\
\hline
\hline
{\scriptscriptstyle 2113} & {\scriptscriptstyle 2296} & {\scriptscriptstyle 1997} & {\scriptscriptstyle 2243} & {\scriptscriptstyle 1308} & {\scriptscriptstyle 9957}
\end{array}
&
\begin{array}{|c|c|c|c|c||c}
\hline
 & {\scriptscriptstyle 1620} &  & {\scriptscriptstyle 553} & {\scriptscriptstyle 236} & {\scriptscriptstyle 3424} \\
\hline
{\scriptscriptstyle 986} &  & {\scriptscriptstyle 80} &  &  & {\scriptscriptstyle 1973} \\
\hline
{\scriptscriptstyle 1552} &  & {\scriptscriptstyle 216} & {\scriptscriptstyle 276} &  & {\scriptscriptstyle 2794} \\
\hline
{\scriptscriptstyle 789} & {\scriptscriptstyle 446} & {\scriptscriptstyle 108} &  &  & {\scriptscriptstyle 1461} \\
\hline
 & {\scriptscriptstyle 726} &  & {\scriptscriptstyle 59} & {\scriptscriptstyle 30} & {\scriptscriptstyle 1452} \\
\hline
\hline
{\scriptscriptstyle 4950} & {\scriptscriptstyle 3239} & {\scriptscriptstyle 433} & {\scriptscriptstyle 1106} & {\scriptscriptstyle 1376} & {\scriptscriptstyle 11104}
\end{array}
\\
\end{array}$
\caption{A selection of $ 4 $ partially filled contingency tables yields the barplots above where the first bar corresponds to the Fr\'echet bounds (unfilled table) for the first entry.
Every subsequent bar corresponds to the generalized bounds for the first entry in the table being cumulatively filled by a new entry (the coordinate number of which is below the bar).
Light grey bars represent the maximum, dark grey ones the minimum.
The final partially filled table is displayed below the barplot.
\label{fig:pfillct}}
\end{center}
\end{figure}

\subsection{Network tomography}
\subsubsection{Basics of network tomography}
In the network tomography problems we consider, networks have nodes of two types:
End-nodes, that we associate to computers, and mid-nodes, that we associate to routers.
The routers are supposed to route the traffic in a deterministic way (the route from one computer to another is determined and stays the same over time).
The traffic at each link (computer-router and router-router) is known (the observed data vector $ y $) and the problem is to infer the traffic between each pair of computers (including a computer to itself via a router).
We assume there are less links than pairs of computers (a reasonable assumption), so the problem is under-determined.
In our example (see figure \ref{fig:2rout}) the $ 8 $ computers are represented by the letters $ (a,b,c,d,e,f,g,h) $ and the $ 2 $ routers are $ (r1, r2) $.
The $ 18 $ edges are ordered by computer-router first, then router-computer and router-router, lexicographically in each category:
\[
\begin{array}{l}
(a\_r1, b\_r1, c\_r1, d\_r1, \,\,\, e\_r2, f\_r2, g\_r2, h\_r2,\dots \\
\dots r1\_a, r1\_b, r1\_c, r1\_d, \,\,\, r2\_e, r2\_f, r2\_g, r2\_h,\dots \\
\multicolumn{1}{r}{\dots r1\_r2, r2\_r1)}
\end{array}
\]
The $ 64 $ pairs of computer are ordered lexicographically $ (a\_a, a\_b, \dots, h\_g, h\_h) $.
The network matrix $ A $ is the incidence matrix between links (rows) and pairs of computers (columns).
So $ A[i,j] = 1 $ if link $ i $ belongs to the route of the computer pair $ j $, and $ A[i,j] = 0 $ otherwise.

There are dependence relations due to the conservation of flow at each router (the sum of what comes in the router equals the sum of what goes out).
This translates into a rank $ 16 $ for our $ 18 $ by $ 64 $ matrix $ A $.
We can therefore eliminate exactly one link per router and recover its traffic from the flow conservation equation.
Here we deleted rows $ 12 $ and $ 16 $ (links $ r1\_d $ and $ r2\_h $) of the matrix and of the vector $ y $.
The resulting $ 16 $ by $ 64 $ matrix has now full rank.

We borrowed our example and the data from \cite{Cao2000pap}.
They gathered link traffic on a $2$-router network over $ 288 $ time slots spaced out by a period of $ 5 $ minutes.
Unfortunately their data is incomplete since there is no record of traffic on the links $ r1_r2 $ and $ r2_r1 $ (and indeed the $ 16 $ by $ 64 $ matrix resulting from removing the corresponding rows from the incidence matrix has rank $ 15 $).

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.6]{netw_2rout.pdf}}
\caption{An example of $2$-router network with link traffic (at time $47$).
What are the bounds for the traffic between each pair of computers?
\label{fig:2rout}}
\end{center}
\vskip -0.2in
\end{figure}

To remedy this problem we simulated traffic for this missing data (see figure \ref{fig:2rout}).
The traffic on these two links has a maximum and a minimum.
Let $ S_{out} $ and $ S_{in} $ be respectively the sum of the traffic on the links from $ (a,b,c,d) $ to $ r\_1 $ and from $ r\_1 $ to $ (a,b,c,d) $, and similarly let $ T_{out} $ and $ T_{in} $ be the sum of the traffic on the links from $ (e,f,g,h) $ to $ r\_2 $ and from $ r\_2 $ to $ (e,f,g,h) $.
Then
\[
\max(0, S_{out} - S_{in}) \leq \mathrm{traffic}(r1\_r2) \leq \min(S_{out}, T_{in})
\]
And similarly with $ \mathrm{traffic}(r2\_r1) $.
So we simulated these two traffics by arbitrarily taking the (integer) midpoint of these intervals.
See for example the whole link traffic at time point $ 47 $ (\cite{Cao2000pap} considers $ 288 $ time points) on figure \ref{fig:2rout}.
Next we eliminated rows $ 12 $ and $ 16 $ of the incidence matrix and of the vector $ y $, as mentioned above.
Now we are in the situation of equation \ref{eq:Axy_xnn} with a full rank $ 16 $ by $ 64 $ matrix $ A $ and a $ 16 $-dimensional vector $ y $.

\subsubsection{Minimum bound data analysis}
Typical questions of interest are "Is there any pair of computer traffic that stands out from the rest? How much so, when and how often?".
This is a broad question, but in our context minimum bounds that are positive tend to be relatively rare (proposition \ref{prop:nnocutbd}), and are a natural feature for spotting such notable traffic.
We computed the minimum bounds for all the $ 64 $ coordinates at each of the $ 288 $ time points.
Coordinate $ 10 $, corresponding to the pair $ b\_b $ had a positive minimum $ 43 $ times out of $ 288 $.
The next coordinate with most frequent positive minimum ($ 9 $ out of $ 288 $) is coordinate $ 26 $, correpsonding to the pair $ d\_b $.
So the pair $ b\_b $ has a traffic that stands out most often.
The time point at which there are the greatest number of positive minimums is time point $ 47 $ with $ 4 $ positive minimums for pairs $ b\_a, b\_b, b\_c, b\_d $ (see figure \ref{fig:2rout}).
Let's look more closely at that time point.
The bounds for the $ 4 $ pairs are summarized in the following table:
\[
\begin{array}{c||c|c|c|c|}
& \mathrm{b\_a} & \mathrm{b\_b} & \mathrm{b\_c} & \mathrm{b\_d} \\
\hline\hline
\min & 89094 & 437242 & 314699 & 22709 \\
\hline
\max & 198720 & 546868 & 424325 & 132334 \\
\hline
\end{array}
\]

\section{Conclusion}
We described a certain geometric setting to the problem of finding sharp bounds to the coordinates of the solutions to a system (\ref{eq:Axy_xnn}), bounds that we defined as \emph{generalized Fr\'echet bounds}.
This description alone allowed us to easily prove a proposition on the number of positive minimum Fr\'echet bounds.
This allowed us as well to describe a simplified version of the simplex method, and where and how the simplification takes place.
We also discussed how this simplification can significantly lower the complexity of the simplex method.
Finally we demonstrated our algorithm on two relevant inference problems:
\begin{itemize}
\item We synthetized some data for contingency tables that features the sharpness gap between standard Fr\'echet bounds and our generalized Fr\'echet bounds on partially filled tables.
\item We proposed a new direction in network tomography, namely using the (rare) positive minimum Fr\'echet bounds to detect unusual traffic.
We used (incomplete) data from a known dataset (\cite{Cao2000pap}) and spotted several pairs with unusual traffic and a time point with particular unusual traffic.
\end{itemize}
Finally, in this paper, we generalized the notion of Fr\'echet bounds to a system (\ref{eq:Axy_xnn}), showed they are relatively simple to compute, can be much sharper in partially filled tables than usual Frechet bounds, and can be useful in network tomography problems.









\bibliography{../bibtex/gen_frech}
\bibliographystyle{plain}

\end{document}
