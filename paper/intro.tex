\section{Introduction}
\label{sec:intro}
\subsection{Motivations}
\label{sec:motivations}
Cross-classified categorical data is analyzed in tables called \emph{contingency tables} (see for example \cite{Fienberg2007bk}).
Analyzing variable dependence in a such a data set usually requires to compute marginals of this table, like row-sums and column-sums for $2$-way tables.
For multiway tables ($k$-way with $ k \geq 2 $), the marginals to compute are determined by the hypothesis of independence (see for example \cite{Dobra2000pap}).
For example with Fisher exact test, one would need to enumerate the tables with those given marginals and compute the p-value of the given table.
More realistically one would uniformly sample a large enough number of these tables to compute an approximate p-value.
Such calculations require some assumption or inference on the distribution of table entries.
In this process it is helpful to have some tight bounds on those entries.
Such bounds, called Frechet bounds, are well-known and are used as well in other contexts, like the theory of copulas (see for example \cite{Joe1997bk}).
In the simpler case of a $2$-way contingency table $(x)_{ij}$, $i = 1,\dots, I $, $ j = 1, \dots, J $, the marginals are $ x_{i+} = \sum_j x_{ij} $ and $ x_{+j} = \sum_i x_{ij} $, and with total count $ N = \sum_{ij} x_{ij} $, the Frechet bounds are
\begin{equation}
\label{eq:Fb}
\max(0, x_{i+} + x_{+j} - N) \leq x_{ij} \leq \min(x_{i+}, x_{+j})
\end{equation}

Beside being very easy to compute, Frechet bounds are sharp.
That is, for every $ i,j $, there is a table $ x $ where $ x_{ij} $ is equal to the lower bound and there is a table $ x' $ where $ x'_{ij} $ is equal to the upper bound.
It turns out that the set of all tables with fixed marginals is a polytope $ P $ of dimension $ (I-1)*(J-1) $ (see section \ref{sec:polyh_geom}).
So from a geometric perspective, Frechet bounds define a tight bounding box of dimension $ I*J $ that contains $ P $ as illustrated in figure \ref{fig:bound_box}.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.4]{bound_box_new.pdf}}
\caption{Frechet bounds define a tight bounding box around the solution polytope $ P $.
\label{fig:bound_box}}
\end{center}
\vskip -0.2in
\end{figure} 

However, if the table contains structural zeros, that is, if it is known that some entries are always $ 0 $, or equivalently if the table is partially filled, Frechet bounds fail to be sharp.
All this generalizes without major difficulty to multi-way tables.

Contingency tables with given marginals are solutions of an under-determined linear system
\begin{equation*}
\label{init_syst}
Ax = y \qquad x \in \mathbb{N}^n
\end{equation*}
where $ x $ is the vectorized list of table entries, $y$ is the vectorized list of marginal entries, and $ A $ is the incidence matrix between the table entries and the marginals.
That is, $ A[i,j] = 1 $ if $ x[j] $ contributes to the marginal entry $ y[i] $, and $ A[i,j] = 0 $ otherwise.
Since some marginal entries can be recovered from the rest, some rows can be eliminated so that $ A $ is a full rank $m$ by $n$ matrix.
Many other problems are similar in nature and form a linear sub-class of a larger class called \emph{ill-posed inverse problems}.
They are inverse problems because one tries to infer information about the object (here the table) from observations (marginal counts).
They are ill-posed because there are usually more than one solution (or no solution at all, i.e. the problem is not \emph{feasible}).
Such problems include contingency tables with given marginals, including tables with structural zeros (see \cite{Chen2007pap}), network tomography (see \cite{Vardi1996pap}, \cite{Castro2004pap}), mass transportation problems (see \cite{Rachev1999pap}), networks with given degree vectors (see \cite{Blitzstein2011pap}, \cite{Chatterjee2010pap}), and flux-balance analysis (see \cite{Orth2010prim}).
All these problems have a solution set that is a polyhedron, usually bounded, and usually in high dimension.
In most of these problems statistics related to the coordinates are of great interest, in particular their range, which is given by the Frechet bounds.

From a linear-algebraic point of view, these problems call for using an ``on the shelf'' LP (Linear Programming) solver to get these bounds.
This is certainly not as fast as the formula \ref{eq:Fb}, but LP solvers are so prevalent, convenient, and usually so well optimized, that they make this Frechet bound generalization remakably easy and relatively efficient (contrast for example with \cite{Dobra2010pap}).
Finally, notice that, to the author's knowledge, very few simple algorithms have been proposed to compute such generalized Frechet bounds.

From a geometric point of view each coordinate $ x[i] $ is associated with a coordinate hyperplane ($ \{x:x[i] = 0\}) $).
Some hyperplanes support facets of the polyhedron, and some do not.
We will see that positive Frechet bounds are always associated with the former, but zero Frechet bounds are can be associated with both.
We then generalize also zero minimum Frechet bounds by distinguishing those associated with facets and those not.

\subsection{Setting and results}
\label{sec:results}
General systems of interest to which we apply the notion of generalized Fr\'echet bounds are defined as the solution set of
\begin{equation}
\label{eq:Axy_xnn}
Ax = y \qquad x \geq 0
\end{equation}
Where $ A $ is a given $m$ by $n$ matrix, $ m < n $, and $ y $ a given vector (typically, the ``vector of observations'').
Moreover we assume that the system is feasible (clearly it is for the contingency table problem since we are given one solution), and that $ A $ has full rank.
The latter assumption might entail eliminating some rows if necessary:
For example in $2$-way $ I $ by $ J $ tables we know the total number of observations from the sum of the $I$ row-marginals.
Therefore given $J-1$ column marginals one can infer the last one.
That is, for $ A $ to be full rank, its number of rows $m$ is $ I + J - 1 $.

In the contingency table problem, the solutions $ x $ are counts and therefore integers.
For simplicity of exposition we consider here real solutions.

Such a solution set is called a (convex) \emph{polyhedron} (the intersection of finitely many half-spaces).
In most problems it is bounded; it is then called a (convex) \emph{polytope} (see \cite{Fukuda2004lk}).
We will write it $ P(A,y) $, or simply $ P $.
For example in the contingency table problem, cell values $ x[i] $ cannot run to infinity.
%Our algorithm \ref{algo:getgFb} and theorem \ref{prop:nposfb} can be adapted to the unbounded case, but for simplicity and greater interest we decided to state and describe them for the bounded case.
Finally the affine space $ \mathcal{A} $ defined by $ Ax = y $ (without the constraint $ x \geq 0 $) will be called the \emph{solution space}.

To each coordinate $ x[i] $ is associated the hyperplane $ H_i $ of $ \mathcal{A} $ defined by $ \{x: x[i] = 0\} \cap \mathcal{A} $, that is, the intersection of one of the side of the positive orthant with $ \mathcal{A} $.

\begin{define}
\label{def:redundant}
A hyperplane $ H_i $, and its associated coordinate $ x[i] $, are said to be \emph{structural} for the polyhedron $ P $ if $ H_i $ supports a facet of $ P $. \\
Otherwise they are called \emph{redundant}.
\end{define}

\begin{define}
\label{def:Fbd}
The minimum and maximum values of $ x[i] $ on a polyhedron $ P $ defined by (\ref{eq:Axy_xnn}) are called \emph{generalized Frechet bounds}, or now just \emph{Frechet bounds}. \\
If a Frechet minimum bound for $ x[i] $ is zero and
\begin{itemize}
\item $ x[i] $ is a redundant coordinate, we call it a \emph{$ 0_+ $ minimum (Frechet) bound}.
\item $ x[i] $ is a structural coordinate, we call it a \emph{$ 0_- $ minimum (Frechet) bound}.
\end{itemize}
\end{define}

\begin{define}
A system \ref{eq:Axy_xnn} is said to be \emph{minimal} if it is not set with redundant coordinates.
\end{define}

We can now state our results in terms of Frechet bounds:
\begin{prop}
\label{prop:n0+bd}
In an $ m $ by $ n $ system (\ref{eq:Axy_xnn}), the number of positive or $ 0_+ $ minimum Frechet bounds is at most $ m $. \\
If the system defines a bounded polyhedron, then the number of positive or $ 0_+ $ minimum Frechet bounds is at most $ m-1 $.
\end{prop}

\begin{prop}
\label{prop:minzervert}
Let $ v $ be a solution of an $ m $ by $ n $ system (\ref{eq:Axy_xnn}) with exactly $ n-m $ zero coordinates.
Then the minimum Frechet bounds for these coordinates are all $ 0_- $.
\end{prop}

\begin{prop}
\label{prop:allFbd}
Given an $ m $ by $ n $ system (\ref{eq:Axy_xnn}) and a Linear Program (LP) solver, algorithm \ref{algo:allFbd} from section \ref{sec:loopFbd} returns the list of all Frechet bounds (distinguishing between $ 0_+ $ and $ 0_- $ bounds). \\
If $ c(LP) $ is the running time of the LP solver, the running time of algorithm \ref{algo:allFbd} is $ O(n\, c(LP)) $
\end{prop}

\begin{prop}
\label{prop:simplif_Axy}
Given an $ m $ by $ n $ system (\ref{eq:Axy_xnn}) and its list of Frechet bounds, algorithm \ref{algo:remred} simplifies it to a minimal system. \\
The running time of algorithm \ref{algo:remred} is $ O(m) $.
\end{prop}

In sections \ref{sec:polyh_geom} and \ref{sec:polyh_alg} we describe the geometry and the linear algebra of the solution set.
The geometry section is sufficient to prove proposition \ref{prop:n0+bd}.
We then explain in section \ref{sec:redundance} the geometric motivation to separate Frechet $ 0 $ minimum bounds into $ 0_+ $ and $ 0_- $ bounds.
Section \ref{sec:algremH1} shows how to use any linear programing (LP) solver to detect $ 0_+ $ from $ 0_- $ bounds and section \ref{sec:genFbalgo} ties everything together into the algorithms that outputs all the Frechet bounds and simplifies the initial linear system.
Finally section \ref{sec:experim} is devoted to an illustration of the sharpness of generalized Frechet bounds (section \ref{sec:partfilledct}) and to an application to unusual traffic in the field of network tomography (section \ref{sec:netwtom}).

We use the standard notation of the $R$ programming language (which we used to code our applications) for vectors $ v $ and their entries $ v[i] $, and for matrices $ M $ and their rows $ M[i,] $, columns $ M[,j] $, etc.
