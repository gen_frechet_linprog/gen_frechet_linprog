\documentclass{beamer}
%\usepackage{color}
%\setbeamertemplate{navigation symbols}{}


\usetheme{Boadilla}
\useoutertheme{infolines}


\begin{document}
\newcommand{\vectwo}[2]{
    \left(\begin{array}{c}
        {#1} \\
        {#2}
    \end{array}\right)
}
\newcommand{\vol}[2]{\mbox{Vol}_{#1}({#2})}
\newcommand{\sign}{\operatorname{sign}}
\newcommand{\inv}{\operatorname{inv}}
\newcommand\Tstrut{\rule{0pt}{3.5\normalbaselineskip}}
\newcommand\Bstrut{\rule[-0.5\normalbaselineskip]{0pt}{0pt}}

\title{Generalized Frechet Bounds}
\author{Bertrand Haas \& Edoardo Airoldi\\ Statistics department, Harvard Univiversity}
\date{\today} 
\begin{frame}
\titlepage
\end{frame}

%\begin{frame}\frametitle{Table of contents}\tableofcontents
%\end{frame} 


\section{Introduction}
\begin{frame}\frametitle{Linear ill-posed inverse problems}
Variety of problems with a solution set that is a polyhedron $ P $ defined by
\[
A x = b,\quad x \geq0
\]
Where $ A $ is a full rank $ m $ by $ n $ matrix with $ m < n $.
Typical problems:
\begin{itemize}
\item Optimize a function on $ P $.
\item Efficiently sample points from $ P $.
\item Get the exact or approximate size of $ P $.
\end{itemize}
Examples:
\begin{itemize}
\item Independence or not of categorical variables presented in a contingency table.
\item Same, but with structural zeros in the table.
\item Network tomography
\end{itemize}
\end{frame}

\begin{frame}\frametitle{The usual contingency table problem}
Consider a system with two variables snp = 0 or 1, and pheno = I, II, or III.
The property of interest is the independence or not of snp and pheno.\\
$ 43 $ subjects cross-classified:
\begin{center}
\scriptsize
\begin{tabular}{c||c|c|c||c}
& pheno. I & pheno. II & pheno. III & Row margin.\\
\hline\hline
snp = 0 (control) & 9 & 12 & 7 & 28 \\
\hline
snp = 1 & 4 & 4 & 7 & 15 \\
\hline\hline
Col. margin. & 13 & 16 & 14 & 43
\end{tabular}
\normalsize
\end{center}
Assume the number of variables for each value is given (the row and column marginals).
Many different tables with those marginals, distributed according to the relative dependence of \textit{snp} and \textit{pheno}:
\begin{center}
\scriptsize
\begin{tabular}{c||c|c|c||c}
& pheno. I & pheno. II & pheno. III & Row margin.\\
\hline\hline
snp = 0 & 0 & 16 & 12 & 28 \\
\hline
snp = 1 & 13 & 0 & 2 & 15 \\
\hline\hline
Col. margin. & 13 & 16 & 14 & 43
\end{tabular}
\normalsize
\end{center}
The place of the given table in the set of all possible tables with those marginals gives an clue about the (in)-dependence of \textit{snp} and \textit{pheno}.
\end{frame}

\begin{frame}\frametitle{Frechet bounds}
One useful statistic to gather among the set of all tables $ x $ with given marginals $ x_{i+} $ and $ x_{+j} $ is the range of the entries $ x_{ij} $ in the table.
This is given by the Frechet bounds:
\begin{block}{Frechet bounds}
\[
\max(0, x_{i+} + x_{+j} - N) \leq x_{ij} \leq \min(x_i+, x_+j)
\]
\end{block}
Where $ N = \sum x_{ij} $.
These are sharp bounds:
For each Frechet bound $ f_{ij} $, there is a table with $ x_{ij} = f_{ij} $.\\
\pause Suppose now there are two mutually exclusive values for the variables.
For example suppose $ \mathit{pheno.}\, I $ never occurs with $ \mathit{snp} = 1 $, so $ x_{2,1} = 0 $.
This is called a structural zero. \\
\pause Now {\color{red} Frechet bounds not sharp anymore}. \\
Is there a {\color{red} simple way} to get again sharp bounds?
\pause Not known.
\end{frame}


\section{Geometry of the system}
\begin{frame}\frametitle{The matrix form of the problem}
\begin{center}
\footnotesize
\begin{tabular}{c||c|c|c||c}
& pheno. I & pheno. II & pheno. III & Row margin.\\
\hline\hline
control & $ x[1] $ & \only<1>{$ x[3] $}\alt<2->{\normalsize \color{red} $ x[3] $ \scriptsize} & $ x[5] $ & 28 \\
\hline
snp & $ x[2] $ & $ x[4] $ & $ x[6] $ & 15 \\
\hline\hline
Col. margin. & 13 & 16 & 14 & 43
\end{tabular}
\normalsize
\end{center}
Such contingency tables $ x $ satisfy the system $ Ax = b $, $ x \geq 0 $ where
\footnotesize
\[
\overbrace{\left(
\begin{array}{cccccc}
 1 & 1 &   &   &   &   \\
   &   & 1 & 1 &   &   \\
   &   &   &   & 1 & 1 \\
 1 &    & 1  &    &  1
\end{array} \right)}^{\textstyle A}
\left(\begin{array}{c}
    x[1] \\ \dots \\ \\ x[6]
\end{array}\right) =
\overbrace{\left(
\begin{array}{c}
13 \\ 16 \\ 14 \\ 28
\end{array} \right)}^{\textstyle b}
\]
\normalsize
Before trying to do anything with such a problem, can we simplify this setting?\\
\pause
We will see that $ x[3] $ is actually \emph{redundant} (``useless'') for our purpose and the system can be simplified from a $ 4 \times 6 $ to a $ 3 \times 5 $ system.
\end{frame}

\begin{frame}\frametitle{Generalized Frechet bounds}
The matrix form of the contingency table problem yields naturally to a generalization of Frechet bound to any linear ill-posed inverse problem $ Ax = b, x \geq 0 $:
What are sharp bounds for the $ x[i] $'s, i.e., what is a tight box around the polyhedron $ P $.
\begin{figure}
\includegraphics[scale=0.4]{bound_box_01.pdf}
\end{figure}
Once a problem is in this matrix form, ``easy'' to find the bounds:
Use any LP solver on the linear functions $ f_i(x) = x[i] $ to get their minimum and maximum.
Can we do better?
\end{frame}

\begin{frame}\frametitle{The geometry of the system}
View $ P = \{x : Ax = b, x \geq 0 \} $ as the intersection $ \mathcal{A} \cap \mathcal{C}_{(+, \dots, +)} $ where
\begin{itemize}
    \item $ \mathcal{A} = \{x : Ax = b\} $ is the \emph{solution} affine space,
    \item $ \mathcal{C}_{(+, \dots, +)} $ is the positive orthant (cone).
\end{itemize}
(we assume $ P $ has full dimension $ d = n - m $). Let
\begin{center}
\vspace{-2em}
\begin{eqnarray*}
H_i &=& \{x \in \mathcal{A}: a[i] = 0\} \\
\mathcal{H} &=& \{H_i\} \quad\mbox{an \emph{arrangement of hyperplanes}}\\
H_i^+ &=& \{x \in \mathcal{A}: a[i] \geq 0\}
\end{eqnarray*}
\end{center}
So $ P = \cap_{i=1}^{n} H_i^+ $.
\pause
In our example:
\footnotesize
\[
P = \Bigg\{\overbrace{\left(\begin{array}{cccccc}
 1 & 1 &   &   &   &   \\
   &   & 1 & 1 &   &   \\
   &   &   &   & 1 & 1 \\
 1 &    & 1  &    &  1
\end{array}\right)}^{\textstyle A}
\left(\begin{array}{c}
 x[1] \\ \vdots \\ x[5] \\ x[6]
\end{array}
\right) =
\overbrace{\left(\begin{array}{c}
 13 \\ 16 \\ 14 \\ 28
\end{array}\right)}^{\textstyle b},
x \geq 0
\Bigg\}
\]
\normalsize
$ \mathcal{A} $ and $ P $ have dimension $ d = 6-4 = 2 $ and $ \mathcal{H} $ is an arrangement of lines.
Vertexes (of $ \mathcal{H} $ or $ P $) are the intersection of $ 2 $ lines.

\end{frame}

\begin{frame}
\begin{figure}
\includegraphics[scale=0.4]{sol_space_n6.pdf}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\includegraphics[scale=0.4]{mini_hypl_arr_n6.pdf}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\includegraphics[scale=0.4]{nobound_hypl_n6.pdf}
\end{figure}
\end{frame}

\begin{frame}\frametitle{Minimum Frechet bounds}
$ \max(0, x_{i+} + x_{+j} - N) \leq x_{ij} \leq \min(x_{i+}, x_{+j}) $
\begin{center}
\scriptsize
\begin{tabular}{c||c|c|c||c}
Min. $x[k]$ & pheno. I & pheno. II & pheno. III & R\\
\hline\hline
control & 0 & {\color{red} 1} & 0 & 28 \\
\hline
snp & 0 & 0 & 0 & 15 \\
\hline\hline
C & 13 & 16 & 14 & $ N = 43 $
\end{tabular}
\normalsize
\end{center}
Here $ H_3 $, therefore $ x_{1,2} = x[3] $, is ``redundant''.
Geometrically $ P = \cap_{i \not = 3} H_i^+ $.
\begin{block}{Definition}
A hyperplane $ H_i $ (and its corresponding coordinate $ x[i] $) is called \emph{redundant} if it does not contribute to the structure of $ P $.\\
Otherwise it is called a \emph{structural} hyperplane (coordinate).
\end{block}
\end{frame}

\begin{frame}{More-generalized Frechet bounds}
What if $ H_3 $ in the example was $ 1 $ unit further in?\\
A zero bound is not enough to ensure $ x[i] $'s non-redundancy.
\begin{block}{Definition}
A zero minimum Frechet bound for $ x[i] $ is a
\begin{itemize}
\item $ 0_+ $ if $ x[i] $ is redundant
\item $ 0_- $ if $ x[i] $ is structural
\end{itemize}
\end{block}
How can we check if a $ 0 $ minimum bound is a $ 0_- $ or a $ 0_+ $?\\
LP solvers output the optimum of the objective function together with a vertex (where the optimum is attained).
That can help.
\end{frame}

\begin{frame}{Gathering more info for less LP solving}
\begin{block}{Proposition}
If $ v $ is a vertex of $ P $ with exactly $ d $ zero coordinates, then these coordinates are all structural.
\end{block}
\textbf{Proof:}
$ v $ is the intersection of at least $ d $ facets of $ P $, each suported by some $ H_i $ therefore $ v $ is the intersection of exactly $ d $ facet of $ P $ ($ v $ is \emph{simple}).
So each of the $ H_i $ supports a facet of $ P $, that is, each of the $ H_i $ is structural.$\Box$\\
\vspace{1em}
In our $ 2-D $ example there are too many ``good vertices'', with only $ 2 $ zero coordinates (simple vertexes).
We need at least a $3$-dimensional example for non-simple vertexes. \\
Consider the system
\scriptsize
\begin{equation}
\label{eq:3d6h1r}
\left(\begin{array}{cccccc}
 1 & 1 & 1 &   &   & 2 \\
   & 1 &   & 1 &   & 1 \\
   &   & 1 &   & 1 & 1
\end{array}\right) x =
\left(\begin{array}{c}
 2 \\ 1 \\ 1 \\
\end{array}\right),\quad x \geq 0
\end{equation}
\normalsize
\end{frame}

\begin{frame}
\begin{figure}
\includegraphics[scale=0.4]{3D_6H_1R.pdf}
\end{figure}
\end{frame}

\begin{frame}
The LP solver might output the following vertexes:
\scriptsize
\begin{equation}
\label{eq:Pminvert}
\begin{array}{c|cccccc}
\begin{array}{c}\mbox{Coordinate}\\\mbox{minimized}\end{array}
& x[1] & x[2] & x[3] & x[4] & x[5] & x[6] \\
\hline
\begin{array}{c}\mbox{at}\\\mbox{Vertex}\end{array} &
\Tstrut \left(\begin{array}{c}
\mathbf{0_?} \\ 0 \\ 0 \\ 0 \\ 0 \\ 1
\end{array}\right) &
\left(\begin{array}{c}
2 \\ \mathbf{0_-} \\ 0_- \\ 1 \\ 1 \\ 0_-
\end{array}\right) &
* &
\left(\begin{array}{c}
0 \\ 1 \\ 1 \\ \mathbf{0_?} \\ 0 \\ 0
\end{array}\right) &
\left(\begin{array}{c}
0 \\ 1 \\ 1 \\ 0 \\ \mathbf{0_?} \\ 0
\end{array}\right) \Bstrut& * \\
\hline
v^{(i)} & v^{(1)} & v^{(4)} & * & v^{(0)} & v^{(0)} & *
\end{array}
\end{equation}
\normalsize
It is still not enough!
We can't figure out some of the $ 0 $-bounds. \\
\pause
Solution:  Remove $ H_i $. \\
If redundant, no effect on $ P $.
So minimum remain $ 0 $. \\
If structural, we get a new polytope $ P' = \cap_{j \not = i} H_j $.
Minimum for $ x[i] $ will decrease in $ P' \supset P $, so it will be negative. \\
\pause
Geometrically simple.
Algebraically?
\end{frame}

\begin{frame}\frametitle{Slacking the system}
Permute columns of $ A $ if necessary to decompose $ A $ into blocks $ (A_a, A_b) $ with $ A_a $ invertible.  So
\[
A x = b\,\, \Leftrightarrow \,\, {\color{gray} A_a^{-1} (A_a, A_b) \vectwo{x_a}{x_b} = \overbrace{A_a^{-1} b}^{\textstyle b'} \,\,} \Leftrightarrow \,\, \overbrace{(I_m, Q_{ab})}^{\textstyle A'} \vectwo{x_a}{x_b} = b'
\]
$ x_a $ are the \emph{slack variables}.  Example:
\scriptsize
\begin{align*}
    \left(\begin{array}{ccc|ccc}
        1 & 1 & 1 &   &   & 2 \\
          & 1 &   & 1 &   & 1 \\
          &   & 1 &   & 1 & 1
    \end{array}\right)
    &
    \left(\begin{array}{c}
        x[1] \\ \vdots \\x[5] \\ x[6]
    \end{array}\right) 
    =
    \left(\begin{array}{c}
            2 \\ 1 \\ 1
    \end{array}\right)
    \mapsto
    \dots \\
    \dots
    &\mapsto
    \left(\begin{array}{ccc|ccc}
        1 &   &   &-1 &-1 &   \\
          & 1 &   & 1 &   & 1 \\
          &   & 1 &   & 1 & 1
    \end{array}\right)
    \left(\begin{array}{c}
        x[1] \\ \vdots \\x[5] \\ x[6]
    \end{array}\right) =
    \left(\begin{array}{c}
            0 \\ 1 \\ 1
    \end{array}\right)
\end{align*}
\normalsize
\end{frame}

\begin{frame}\frametitle{Projection on a hyperplane of coordinates}
We get a base $ (Q_a, Q_b) $ of $ \mathbb{R}^n $, with $ Q_a \perp Q_b $ and $ Q_b $ a base for $ \vec{\mathcal{A}} = \mbox{null}(A) $:
\[
\footnotesize
\left(\begin{array}{cc}
I_m & Q_{ab} \\
Q_{ab}^T & -I_d
\end{array}\right) =
\overbrace{\left(\begin{array}{ccc|}
\alt<2->{{\color{red}\makebox[20pt][l]{\rule[0.1cm]{0.35\textwidth}{0.8pt}}}%
        {\color{red}\makebox(-16,-50){\rule[-1ex]{0.8pt}{6\normalbaselineskip}}}%
   &   &   }%
{1 &   &   }\\
1 &   &     \\
  & 1 &     \\
\hline
-1& 1 &     \\
-1&   & 1   \\
  & 1 & 1
\end{array}\right. }^{\textstyle Q_a = A'^T}
\overbrace{\left. \begin{array}{ccc}
-1 &-1 &   \\
 1 &   & 1 \\
   & 1 & 1 \\
\hline
-1 &   &  \\
   &-1 &  \\
   &   &-1
\end{array}\right)}^{\textstyle Q_b}
\normalsize
\]
\pause
Since $ x[i] $ is useless, strike out row and column $ i $ (project $ P $ on $ H_i $) and recover a simpler system:
\[
\footnotesize
\overbrace{\left(\begin{array}{cc|ccc}
1 &   & 1 &   & 1 \\
  & 1 &   & 1 & 1
\end{array}\right)}^{\textstyle \tilde{A}}
\overbrace{\left(\begin{array}{c}
x[1] \\ x[2] \\ x[5] \\ x[6] \\ x[4]
\end{array}\right)}^{\textstyle \tilde{x}} =
\overbrace{\left(\begin{array}{c}
 1 \\ 1
\end{array}\right)}^{\textstyle \tilde{b}}
\normalsize
\]
\end{frame}

\begin{frame}\frametitle{The algorithm}
\begin{itemize}
\item Input:  Matrix $ A $, vector $ b $ from $ Ax = b , x \geq 0$
\item Get $ A' = (I_m, Q_{ab}) $ and $ b' $ so system is equivalent to $ A' x = b' $
\item For $ i = 1:n $
\item pivot $ (1,i) $ in $ (A', b') $
\item erase row $ 1 $ and column $ 1 $ of $ (A',b') $ to get system $ (\tilde{A}, \tilde{b}) $.
\item Use any LP solver to solve:
\begin{itemize}
\item Minimize $ Q_{ab}[1,]x_b - b'[1] $ subject to
\item $ \tilde{Q_{ab}} x_b \leq \tilde{b} $
\end{itemize}
\item If minimum $ < 0 $, then FB for $ x[i] $ is a $ 0_- $
\item If minimum $ == 0 $, then FB is a $ 0_+ $
\item If minimum $ > 0 $ then FB is the minimum found.
\end{itemize}
\end{frame}












\begin{frame}\frametitle{How many useless coordinates and how often?}
Experimentally on a network tomography problem equivalent to a series of $ 288 $ ($ 4 \time 4 $)-contingency tables:  $~1/4$ of the time there was $ 1 $ to $ 4 $ useless coordinates.\\
\pause
Column space perspective: $ Ax = b $, $ x \geq 0 $ means $ b $ is in the cone generated by the column vectors of $ A $.\\
Fix $ A $ and allow to rescale $ b $ (with $ x $ ) so that it lies on the unit sphere $ S_1 $.
And assume $ b $ has uniform distribution on $ S_1 $. \\
What are the chances that $ Ax = b $ has some useless coordinates? \\
\pause
Geometric picture.
\end{frame}

\begin{frame}\frametitle{Row space perspective}
\begin{figure}
\includegraphics[scale=0.8]{min_max_y_x1.pdf}
\end{figure}
\end{frame}

\begin{frame}
\begin{columns}
\begin{column}{5cm}
The probability of getting a useless coordinate is greater or equal to the ratio
\[
\frac{\vol{m-1}{C} - \vol{m-1}{\hat{C}}}{\vol{m-1}{C}}
\]
As $ n $ increases ($ m $ fixed) the ration gets smaller.\\
As $ m $ increases ($ n > m $ fixed) the ration gets larger.
\end{column}
\begin{column}{5cm}
\begin{figure}
\includegraphics[scale=0.8]{colsp_polint.pdf}
\end{figure}
\end{column}
\end{columns}
\end{frame}


\begin{frame}\frametitle{Conclusion}
\begin{itemize}
\item For any ill-posed inverse problem of the form $ Ax = b $, $ x \geq 0 $, where $ A $ is full rank, $ m < n $, we have a way to detect and remove ``useless'' coordinates and therefore get a \emph{minimal system} $ \tilde{A} \tilde{x} = \tilde{b} $ with $ \tilde{A} $ not larger than $ A $.
\pause
\item In general the detection can be done by half the simplex method (set $ x[i] = 0 $ and check if the resulting system is feasible).
\pause
\item It is not very useful in general for linear programming, but is worth it for more complex objective functions, for sampling and for computing volumes.
\pause
\item In the particular setting of contingency tables, the detectiom cost is $ O(n) $.
\pause
\item We use it in our iterative method for sampling points from $ P $.
\item With a probability distribution on $ b $, we can quantify the probability of getting useless coordinates.
\end{itemize}
\end{frame}


\end{document}
