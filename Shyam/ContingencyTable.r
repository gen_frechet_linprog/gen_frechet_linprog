runSim <- function(x_o=1,h=4,w=4){
## load required libraries:
library(lpSolve)

## create Contingency table:
A <- NULL
for(i in 1:h){
for(j in 1:w){
v <- rep(0,(h+w)+(h*w))
v[i] <- 1
v[h+j] <- 1
v[(h+w)+((i-1)*h)+j] <- 1
A <- cbind(A,v)
}
}
## create marginals:

y <- A%*%floor(runif(h*w,min=0,max=999))
y <- c(y[1:(h+w)],rep(0,h*w))

y_1 <- y

A_1 <- A
Max <- rep(0,h*w)
Min <- rep(0,h*w)

## pick the coordinate for which we will find bounds
order <- NULL
available <- (1:(h*w))[-x_o]
objective <- rep(0,h*w)
objective[x_o] <- 1
values <- NULL
## find Initial Bound:

Max[1] <- lp("max",objective,A_1,c(rep("=",(h+w)),rep(">=",length(y_1[-(1:(h+w))]))),y_1)$solution[which(objective==1)]
Min[1] <- lp("min",objective,A_1,c(rep("=",(h+w)),rep(">=",length(y_1[-(1:(h+w))]))),y_1)$solution[which(objective==1)]


for(i in 2:(h*w)){
## pick the next coordinate to fix
nextCoodNum <- floor(runif(1,min=1,max=length(available)))
nextCood <- available[nextCoodNum]
order <- c(order,nextCood)
if(nextCood > x_o){
coordinatePosition <- which(available==nextCood)+1
}else{
coordinatePosition <- which(available==nextCood)
}
## find bounds for coodrinate
rangeObjective <- rep(0,ncol(A_1))
rangeObjective[coordinatePosition] <- 1
nextMax <- lp("max",rangeObjective,A_1,c(rep("=",(h+w)),rep(">=",length(y_1[-(1:(h+w))]))),y_1)$solution[coordinatePosition]
nextMin <- lp("min",rangeObjective,A_1,c(rep("=",(h+w)),rep(">=",length(y_1[-(1:(h+w))]))),y_1)$solution[coordinatePosition]

## fix the coordinate
values <- c(values,floor(runif(1,min=nextMin,max=nextMax)))

## remove the coordinate from our matrix:
available <- available[-nextCoodNum]
y_1 <- y_1 - values[i-1]*A_1[,coordinatePosition]
y_1 <- y_1[-((h+w)+coordinatePosition)]
A_1 <- A_1[,-coordinatePosition]
A_1 <- A_1[-((h+w)+coordinatePosition),]
objective <- objective[-coordinatePosition]

## calculate conditional range for x_o:
Max[i] <- lp("max",objective,A_1,c(rep("=",(h+w)),rep(">=",length(y_1[-(1:(h+w))]))),y_1)$solution[which(objective==1)]
Min[i] <- lp("min",objective,A_1,c(rep("=",(h+w)),rep(">=",length(y_1[-(1:(h+w))]))),y_1)$solution[which(objective==1)]
if(Max[i] == Min[i]) break
}
cbind(c(x_o,order[1:i]),Max[1:i],Min[1:i],values[1:i]) -> result

return(result[-nrow(result),])
}