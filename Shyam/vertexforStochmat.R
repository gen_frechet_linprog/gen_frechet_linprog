library(rcdd)
## read Stoch matrix
x <- read.table("stochmat.txt") 
x <- as.matrix(x)

## Reduce Matrix so that it has full-row rank

 X <- x[qr(t(x))$pivot[c(1:qr(t(x))$rank)],]

## Create Y
Y_o <- rowSums(X)

## Create Identity matrix
I<-diag(ncol(X))

## Make Hyper Plane Representation
H<-makeH(-I,c(rep(0,94)),X,Y_o)

## Convert to Vertex representation
V<-scdd(d2q(H))