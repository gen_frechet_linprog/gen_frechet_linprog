## Load Required Packages
library("rcdd")

## read Stoch matrix
x <- read.table("stochmat.txt") 
x <- as.matrix(x)

## read test matrix

A <- as.matrix(read.table("A.txt"))
y1 <- as.matrix(read.table("y1.txt"))
y2 <- as.matrix(read.table("y2.txt"))
y3 <- as.matrix(read.table("y3.txt"))

## Required Functions for moving through Pseudo vertices
#####################################################################################################################
browser()
## function to find how far we can move along a given vector without making a positive coordinate 0.
findLambda <- function(k,pv,mincoord){
	k_e <- k
	pv_e <- pv
	k <- q2d(k)
	pv <- q2d(pv)
	if(k[mincoord] <= 0 || pv[mincoord] > 0) return("0")
	relevantIndex <- which(k < 0 & pv > 0)
	if(length(relevantIndex) == 0){
		relevantIndex <- which(k > 0 & pv < 0)
		if(length(relevantIndex) == 0) return("0")
		if(length(which(k < 0 & pv == 0)) != 0){ 
			bindingCoordinate <- which(k <0 & pv == 0)[1]
			return(c("zeroBind",bindingCoordinate))
		}
		lambdas <- -(pv[relevantIndex]/k[relevantIndex])
		lambda <- qdq(pv_e[relevantIndex[which(lambdas == max(lambdas))]][1], k_e[relevantIndex[which(lambdas == max(lambdas))]][1])
		lambda <- qneg(lambda)
		return(lambda)
	}
	if(length(which(k < 0 & pv == 0)) != 0){ 
		bindingCoordinate <- which(k <0 & pv == 0)[1]
		return(c("zeroBind",bindingCoordinate))
	}
	lambdas <- -(pv[relevantIndex]/k[relevantIndex])	
	lambda <- qdq(pv_e[relevantIndex[which(lambdas == min(lambdas))]][1], k_e[relevantIndex[which(lambdas == min(lambdas))]][1])
	lambda <- qneg(lambda)
	return(lambda)
}
########################################################################################################

# function to find a direction that gives us the first pseudo vertex that is better than the existing one
greedyFind <- function(Q_2,p,mincoord){
	newPVertex <- NULL
	for(i in 1:ncol(Q_2)){
		lambda <- findLambda(Q_2[,i],p,mincoord)
		if(q2d(lambda) > 0){
			move <- apply(as.matrix(Q_2[,i]),1,"qxq",lambda)
			newPVertex <- qpq(p,move)
			return(newPVertex)
			}
		if(q2d(lambda) < 0){
			return(lambda)
		}
	}
	return("Error")
}

########################################################################################################
# function to find the first working vector for a non-generic vertex:
nonGenericFind <- function(Q_2,p,mincoord,zerosWithOrder){
	newPVertex <- NULL
	bindingCoordinate <- NULL
	coordinateVector <- NULL
	for(i in 1:ncol(Q_2)){
		lambda <- findLambda(Q_2[,i],p,mincoord)
		if(length(lambda) > 1){
			bindingCoordinate <- zerosWithOrder[which(zerosWithOrder == lambda[2])[1]]
			newNonZero <- i + (nrow(Q_2)-ncol(Q_2))
		}else{			
			if(q2d(lambda) > 0){
				move <- apply(as.matrix(Q_2[,i]),1,"qxq",lambda)
				newPVertex <- qpq(p,move)
				return(newPVertex)
			}
		}
	}
	if(length(bindingCoordinate) == 0) return("Error")
	
	return(c("zeroBound",bindingCoordinate,newNonZero))
}
	
########################################################################################################

# function to count number of zero entries in a rational vector
countZeros <- function(v){
	Zeros <- 0
	for(i in 1:length(v)){
		if(v[i] == "0"){
			Zeros <- Zeros + 1
		}
	}
	return(Zeros)
}
########################################################################################################

# function to count number of negative entries in a rational vector

countNegative <- function(v){
	Negatives <- 0
	for(i in 1:length(v)){
		if(qsign(v[i]) == -1){
			Negatives <- Negatives + 1
		}
	}
	return(Negatives)
}

########################################################################################################

# function to give us the  best pseudovertex we can reach by moving along the kernel vectors in Q_2
smartFind <- function(Q_2,p,mincoord){
	newPVertex <- matrix(0,nrow(Q_2),ncol(Q_2))
	negatives <- vector(length=ncol(Q_2))
	mincoordValue <- vector(length=ncol(Q_2))
		for(i in 1:ncol(Q_2)){
			lambda <- findLambda(Q_2[,i],p,mincoord)
			if(q2d(lambda) > 0){
				move <- apply(as.matrix(Q_2[,i]),1,"qxq",lambda)
				newPVertex[,i] <- qpq(p,move)
			}else{
				newPVertex[,i] <- p
			}
			negatives[i] <- countNegative(newPvertex[,i])
			mincoordValue[i] <- newPVertex[mincoord,i]	
		}
	if(min(negatives) < countNegative(p)){
		return(newPVertex[,which(negatives == min(negatives))[1]])
	}else{
		return(newPVertex[,which(mincoordValue == max(mincoordValue))[1]])
	}
}
########################################################################################################

## Function to check co-linearity of two vectors:

isColinear <- function(a,b){
	if((sum(a*b)^2)/(sum(a*a)*sum(b*b)) < 1){
		return(FALSE)
	}
	else{
		return(TRUE)
	}
}
########################################################################################################

## function to check if a pseudo vertex is a vertex:

isVertex <- function(a){
	a <- q2d(a)
	for(i in c(1:length(a))){
		if(a[i] < 0){
			return(FALSE)
		}
	}
	return(TRUE)
}

########################################################################################################

## function to pivot Q_2 into required form

pivotQ_2 <- function(oldZeroIndex,newZeroIndex,Q_2){
	m <- nrow(Q_2) - ncol(Q_2)
	oldZeroRow <- Q_2[oldZeroIndex,]
	if(length(newZeroIndex) > 1){
		Q_2[oldZeroIndex,] <- Q_2[newZeroIndex[which(Q_2[newZeroIndex,oldZeroIndex-m] != 0)][1],]
	}else{
		Q_2[oldZeroIndex,] <- Q_2[newZeroIndex,]
	}
	Q_2[newZeroIndex,] <- oldZeroRow
	if(Q_2[oldZeroIndex,(oldZeroIndex-m)] == 0){	
	return("Error")
	}
	for(i in (1:ncol(Q_2))[-(oldZeroIndex-m)]){
		factor <- qdq(Q_2[oldZeroIndex,i],Q_2[oldZeroIndex,(oldZeroIndex-m)])
		Q_2[,i] <- qmq(Q_2[,i],sapply(Q_2[,(oldZeroIndex-m)],"qxq",factor))
	}

	## normalize Q_2[,(oldZeroIndex-m)]
	Q_2[,oldZeroIndex-m] <- sapply(Q_2[,oldZeroIndex-m],"qdq",Q_2[oldZeroIndex,oldZeroIndex-m])
	return(Q_2)
}

########################################################################################################

## function to pivot a pseudo vertex

pivotP <- function(oldZeroIndex,newZeroIndex,p){
	if(length(newZeroIndex) > 1){
		newZeroIndex <- newZeroIndex[which(Q_2[newZeroIndex,oldZeroIndex-m] != 0)][1]
	}
	temp <- p[oldZeroIndex]
	p[oldZeroIndex] <- p[newZeroIndex]
	p[newZeroIndex] <- temp
	return(p)
}

########################################################################################################
## function to find largest negative entry in a vector
negMax <- function(a){
	if(length(which(a<0)) == 0){return(-1)}
	value <- max(a[which(a < 0)])
	return(which(a == value))
}

########################################################################################################

## this function cycles through the vitrual vertices created by shifting the constraints infinitesmally. It returns a generic pseudovertex

handleNonGeneric <- function(oldZeroIndex,newZeroIndex,Q_2,p_1,perm){
	n <- nrow(Q_2)
	m <- n-ncol(Q_2)
	if(isVertex(p_1)){
		if(length(oldZeroIndex) > 0){
			if(length(newZeroIndex) > 1){
				newZeroIndex <- newZeroIndex[which(Q_2[newZeroIndex,oldZeroIndex-m] != 0)][1]
			}
			p_1 <- pivotP(oldZeroIndex,newZeroIndex,p_1) 
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex,Q_2)
			if(Q_2 == "Error") return("Error") 
			perm <- pivotPerm(oldZeroIndex,newZeroIndex[1],perm)
		}
		return(cbind(perm,p_1,Q_2))
	}
	if(length(oldZeroIndex) > 0){
		
		zerosWithOrder <- newZeroIndex[-1]
		if(length(newZeroIndex) > 1){
			newZeroIndex <- newZeroIndex[which(Q_2[newZeroIndex,oldZeroIndex-m] != 0)][1]
		}
		Q_2g <- pivotQ_2(oldZeroIndex,newZeroIndex[1],Q_2)
		if(Q_2g == "Error") return("Error")
		perm <- pivotPerm(oldZeroIndex,newZeroIndex[1],perm)
		p_g1 <- pivotP(oldZeroIndex,newZeroIndex[1],p_1)
	}else{
		zerosWithOrder <- newZeroIndex
		p_g1 <- p_1
		Q_2g <- Q_2
	}	
	p_g2 <- nonGenericFind(Q_2g,p_g1,negMax(q2d(p_g1)),zerosWithOrder)
	
	while(TRUE){
		if(p_g2[1] == "zeroBound"){
			
			Q_2g <- pivotQ_2(q2d(p_g2[3]),q2d(p_g2[2]),Q_2g)
			if(Q_2 == "Error") return("Error")
			perm <- pivotPerm(q2d(p_g2[3]),q2d(p_g2[2]),perm)
			p_g1 <- pivotP(q2d(p_g2[3]),q2d(p_g2[2]),p_g1)
			zerosWithOrder <- zerosWithOrder[!which(zerosWithOrder == p_g2[2])]
			zerosWithOrder[length(zerosWithOrder)+1] <- p_g2[2]
			p_g2 <- nonGenericFind(Q_2g,p_g1,negMax(q2d(p_g1)),zerosWithOrder)
		}else if(p_g2[1] == "Error"){
			return("Error")
		}else{
			Zeros <- which(p_g2[1:m] == 0)
			if(isVertex(p_g2)){
				if(length(Zeros) > 1){
					Zeros <- Zeros[which(Q_2[Zeros,which(p_g2[m+1:n] != 0)] != 0)][1]
				}
				p_1 <- p_g2
				p_1 <- pivotP(which(p_g2[m+1:n] != 0) + m,Zeros[1],p_1)
				
				Q_2 <- pivotQ_2(which(p_g2[m+1:n] != 0) + m,Zeros[1],Q_2g)
				if(Q_2 == "Error") return("Error")
				perm <- pivotPerm(which(p_g2[m+1:n] != 0) + m,Zeros[1],perm)
				break
			}			
			if(length(Zeros) == 1){
				p_1 <- p_g2
				p_1 <- pivotP(which(p_g2[m+1:n] != 0) + m,Zeros,p_1)
				
				Q_2 <- pivotQ_2(which(p_g2[m+1:n] != 0) + m,Zeros,Q_2g)
				if(Q_2 == "Error") return("Error")
				perm <- pivotPerm(which(p_g2[m+1:n] != 0) + m,Zeros,perm)
				break
			}else{
				return(handleNonGeneric(which(p_g2[m+1:n] != 0)+m,which(p_g2[1:m] == 0),Q_2g,p_g2,perm))
			}
				
		}
	}
	return(cbind(perm,p_1,Q_2))
}
########################################################################################################
pivotPerm <- function(oldZeroIndex,newZeroIndex,perm){
	oldPermIndex <- perm[oldZeroIndex]
	perm[oldZeroIndex] <- perm[newZeroIndex]
	perm[newZeroIndex] <- oldPermIndex
	return(perm)
}
########################################################################################################		
findVertex<- function(X,Q,perm,algorithm="Greedy",Y_o){	
	
	rank <- qr(X)$rank

	## track reduced dimensions:

	n <- ncol(X)
	m <- nrow(X)

	Q_1 <- Q[,1:m]
	Q_2 <- Q[,(m+1):n]
	
	p <- qmatmult(Q,as.matrix(d2q(as.vector(c(Y_o,rep(0,n-m))))))
	
	if(isVertex(p)) return(cbind(perm,p,Q_2))
	
	##return(cbind(Y_o,X))
	while(TRUE){
		## find coordinate to move along:

		## find coordinate
		coordinate <- negMax(q2d(p)) 

		## if more than one equal coordinate, pick one
		coordinate <- coordinate[1]
		
		## if we started with a non Generic pseudo vertex, move to generic:
		if(length(which(p[1:m] == 0)) != 0){
			workingPVertex <- handleNonGeneric(NULL,which(p[1:m] == 0),Q_2,p,perm)
			if(workingPVertex[1] == "Error") return(workingPVertex)
			p <- workingPVertex[,2]
			Q_2 <- workingPVertex[,3:ncol(workingPVertex)]
			perm <- workingPVertex[,1]
			if(isVertex(p)){
				return(cbind(perm,p,Q_2))
			}
		}
		

		## figure out which direction we should move in to maximally reduce our coordinate:

		if(algorithm == "Greedy"){
			p_1 <- greedyFind(Q_2,p,coordinate)
		}else{
			p_1 <- smartFind(Q_2,p,coordinate)
		}	

		## track the coordinate indices:

		oldZeroIndex <- which(p_1[m+1:n] != 0) + m

		newZeroIndex <- which(p_1[1:m] == 0)
		
		## if non-generic, pivot to generic
		if(length(newZeroIndex) != 1){		
			workingPVertex <- handleNonGeneric(oldZeroIndex,newZeroIndex,Q_2,p_1,perm)
			if(workingPVertex[1] == "Error") return(workingPVertex)
			p <- workingPVertex[,2]
			Q_2 <- workingPVertex[,3:ncol(workingPVertex)]
			perm <- workingPVertex[,1]
			if(isVertex(p)){
				return(cbind(perm,p,Q_2))
			}
		}else{
			
			## if we have a vertex return
			if(isVertex(p_1)) return(cbind(perm,p_1,Q_2))
			
			## switch the indicies:
			
			perm <- pivotPerm(oldZeroIndex,newZeroIndex,perm)
			
			## Pivot to get required form of Q_2
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex,Q_2)
			if(Q_2 == "Error") return("Error")
			
			## switch the new zero coordinate with the one that we made non-zero:
			p_1 <- pivotP(oldZeroIndex,newZeroIndex,p_1)
			
			p <- p_1
		}
	}
	return(cbind(perm,p,Q_2))
}	

findBound <- function(x,algorithm="Greedy",Y_o = NULL,bindingCoordinate=1,direction="min"){

## Reduce Matrix so that it has full-row rank
	QR <- qr(t(x))

	redundantRows <- QR$pivot[c((QR$rank)+1:nrow(x))]

	X <- x[QR$pivot[c(1:QR$rank)],]

	## arrange columns in X such that first m are independent

	perm <- qr(X)$pivot

	X <- X[,perm]

	## Remove any columns that are of the form k*C where k is constant and C is another column in the matrix

	rank <- qr(X)$rank

	n <- ncol(X)
	redundantCols <- NULL

	for(i in c((rank+1):n)){
		for(j in c(1:n)){
			if(j!=i & isColinear(X[,j],X[,i])){
				redundantCols <- c(redundantCols,i)
				break()
			}
		}
	}
		
	## update matrix
	if(length(redundantCols) > 1){
		X <- X[,-c(redundantCols[-1])]
	}

	## track reduced dimensions:

	n <- ncol(X)
	m <- nrow(X)

	## Create Y_o if not given
	if(length(Y_o) == 0) Y_o <- rowSums(X)

	## move to exact arithmetic
	X_r <- X
	X <- d2q(X)

	## Create Q such that AxQ = [I,0]

	detQ_1 <- det(q2d(X[,1:rank]))
	
	Q_1 <- solve(q2d(X[,1:rank]))

	Q_1 <- Q_1*detQ_1

	Q_1 <- apply(Q_1,1:2,"round")
	
	Q_1 <- apply(d2q(Q_1),1:2,"qdq",d2q(detQ_1))

	Q_2 <- qneg(qmatmult(Q_1,X[,(rank+1):n]))
	
	Q_22 <- Q_2
	
	Q_11 <- Q_1
	
	Q_1 <- rbind(Q_1,d2q(matrix(0,(n-m),rank)))

	Q_2 <- rbind(Q_2,d2q(diag(n-m)))

	Q <- cbind(Q_1,Q_2)
	
	## find the first vertex
	
	results <- findVertex(X,Q,perm,algorithm,Y_o)
	
	v_1 <- results[,2]
	newPerm <- results[,1]
	Q_2 <- results[,3:ncol(results)]
	
	## get rid on any non zeros after m coordinates:
	oldZeroIndex <- which(v_1[(m+1):n] != 0) + m
	newZeroIndex <- which(v_1[1:m] == 0)
	
	if(length(oldZeroIndex) > 0){
	newPerm <- pivotPerm(oldZeroIndex,newZeroIndex[1],newPerm)
	Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex[1],Q_2)
	v_1 <- pivotP(oldZeroIndex,newZeroIndex[1],v_1)
	}
	
	## move through vertices to reach binding vertex
	if(direction=="max"){
		result <- moveToMax(Q_2,v_1,newPerm,bindingCoordinate)
		Q_2 <- result[,3:ncol(result)]
		v_1 <- result[,2]
		newPerm <- result[,1]
	}else{
		result <- moveToMin(Q_2,v_1,newPerm,bindingCoordinate)
		Q_2 <- result[,3:ncol(result)]
		v_1 <- result[,2]
		newPerm <- result[,1]
	}

	return(cbind(newPerm,v_1,Q_2))
}

moveVertex <- function(Q_2,direction,bC,v){
	constraints <- which(Q_2[-bC,direction] < 0)
	if(length(constraints) == 0) return("Infinite")
	lambdas <- q2d(v[constraints])/q2d(Q_2[constraints,direction])
	lambda <- max(lambdas)[1]
	v_new <- qpq(v,apply(as.matrix(Q_2[,direction]),1,"qxq",d2q(-lambda)))
	return(v_new)
}

moveToMin <- function(Q_2,v_1,newPerm,bindingCoordinate){
	n <- nrow(Q_2)
	m <- n - ncol(Q_2) 
	while(TRUE){
		
		bC <- which(newPerm == bindingCoordinate)
		viableDirections <- findDirections(Q_2,v_1,bC,direction="Min") 
		if(length(viableDirections) == 0){
			## Check if Generic:
			oldZeroIndex <- which(v_1[(m+1):n] != 0) + m
			newZeroIndex <- which(v_1[1:m] == 0)
			if(length(newZeroIndex) >= 1){
				if(length(oldZeroIndex) == 0) oldZeroIndex <- m+1
				result <- cycleMinNonGeneric(newPerm,v_1,Q_2,bindingCoordinate,1,oldZeroIndex)
				Q_2 <- result[,3:ncol(result)]
				v_1 <- result[,2]
				newPerm <- result[,1]
				newZeroIndex <- which(v_1[1:m] == 0)
				if(length(newZeroIndex) >= 1){
					return(cbind(newPerm,v_1,Q_2))
				}
				
			}else{
				return(cbind(newPerm,v_1,Q_2))
			}
		}else{
			v_1 <- moveVertex(Q_2,viableDirections[1],bC,v_1)
			
			## track the coordinate indices:

			oldZeroIndex <- which(v_1[(m+1):n] != 0) + m

			newZeroIndex <- which(v_1[1:m] == 0)
			
			## Pivot Q_2:
			
			if(length(oldZeroIndex) == 0) oldZeroIndex <- m+1
			newPerm <- pivotPerm(oldZeroIndex,newZeroIndex[1],newPerm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex[1],Q_2)
			v_1 <- pivotP(oldZeroIndex,newZeroIndex[1],v_1)
		}	
	}
}

moveToMax <- function(Q_2,v_1,newPerm,bindingCoordinate){
	n <- nrow(Q_2)
	m <- n - ncol(Q_2)
	
	while(TRUE){
		
		bC <- which(newPerm == bindingCoordinate)
		viableDirections <- findDirections(Q_2,v_1,bC,direction="Max")
		if(length(viableDirections) == 0){
			## Check if Generic:
			oldZeroIndex <- which(v_1[(m+1):n] != 0) + m
			newZeroIndex <- which(v_1[1:m] == 0)
			if(length(newZeroIndex) >= 1){
				if(length(oldZeroIndex) == 0) oldZeroIndex <- m+1
				result <- cycleMaxNonGeneric(newPerm,v_1,Q_2,bindingCoordinate,1,oldZeroIndex)
				Q_2 <- result[,3:ncol(result)]
				v_1 <- result[,2]
				newPerm <- result[,1]
				newZeroIndex <- which(v_1[1:m] == 0)
				if(length(newZeroIndex) >= 1){
					return(cbind(newPerm,v_1,Q_2))
				}
				
			}else{
				return(cbind(newPerm,v_1,Q_2))
			}
		}else{
			v_1 <- moveVertex(Q_2,viableDirections[1],bC,v_1)
			
			## track the coordinate indices:

			oldZeroIndex <- which(v_1[(m+1):n] != 0) + m

			newZeroIndex <- which(v_1[1:m] == 0)
			
			## Pivot Q_2:
			if(length(oldZeroIndex) == 0) oldZeroIndex <- m+1
			newPerm <- pivotPerm(oldZeroIndex,newZeroIndex[1],newPerm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex[1],Q_2)
			v_1 <- pivotP(oldZeroIndex,newZeroIndex[1],v_1)
		}	
	}
}



cycleMaxNonGeneric <- function(perm,v,Q_2,bC,count,replacementIndex){
	n <- nrow(Q_2)
	m <- n - ncol(Q_2)
	if(length(v[which(perm==bC)]) == 0) return(cbind(perm,v,Q_2))
	viableDirections <- findDirections(Q_2,v,bC,direction="Max")
	if(length(viableDirections) > 0){
		v <- moveVertex(Q_2,viableDirections[1],which(perm==bC),v)
		## track the coordinate indices:
		oldZeroIndex <- which(v[(m+1):n] != 0) + m
		newZeroIndex <- which(v[1:m] == 0)
		if(length(newZeroIndex) == 1){
			perm <- pivotPerm(oldZeroIndex,newZeroIndex,perm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex,Q_2)
			v <- pivotP(oldZeroIndex,newZeroIndex,v)
			return(cbind(perm,v,Q_2))
		}else{
			perm <- pivotPerm(oldZeroIndex,newZeroIndex[1],perm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex[1],Q_2)
			v <- pivotP(oldZeroIndex,newZeroIndex[1],v)
			return(cycleMaxNonGeneric(perm,v,Q_2,bC,1,oldZeroIndex))
		}
	}else{
		oldZeroIndex <- which(v[(m+1):n] != 0) + m
		newZeroIndex <- which(v[1:m] == 0)
		if(count > length(newZeroIndex)){
			return(cbind(perm,v,Q_2))
		}else{
			
			if(length(oldZeroIndex) == 0) oldZeroIndex <- replacementIndex
			perm <- pivotPerm(oldZeroIndex,newZeroIndex[count],perm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex[count],Q_2)
			v <- pivotP(oldZeroIndex,newZeroIndex[count],v)
			return(cycleMaxNonGeneric(perm,v,Q_2,bC,count+1,replacementIndex))
		}
	}	
}	

cycleMinNonGeneric <- function(perm,v,Q_2,bC,count,replacementIndex){
	n <- nrow(Q_2)
	m <- n - ncol(Q_2)
	if(length(v[which(perm==bC)] == 0)) return(cbind(perm,v,Q_2))
	viableDirections <- findDirections(Q_2,v,bC,direction="Min")
	if(length(viableDirections) > 0){
		v <- moveVertex(Q_2,viableDirections[1],which(perm==bC),v)
		## track the coordinate indices:
		oldZeroIndex <- which(v[(m+1):n] != 0) + m
		newZeroIndex <- which(v[1:m] == 0)
		if(length(newZeroIndex) == 1){
			perm <- pivotPerm(oldZeroIndex,newZeroIndex,perm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex,Q_2)
			v <- pivotP(oldZeroIndex,newZeroIndex,v)
			return(cbind(perm,v,Q_2))
		}else{
			perm <- pivotPerm(oldZeroIndex,newZeroIndex[1],perm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex[1],Q_2)
			v <- pivotP(oldZeroIndex,newZeroIndex[1],v)
			return(cycleMinNonGeneric(perm,v,Q_2,bC,1,oldZeroIndex))
		}
	}else{
		oldZeroIndex <- which(v[(m+1):n] != 0) + m
		newZeroIndex <- which(v[1:m] == 0)
		if(count > length(newZeroIndex)){
			return(cbind(perm,v,Q_2))
		}else{
			if(length(oldZeroIndex) == 0) oldZeroIndex <- replacementIndex
			perm <- pivotPerm(oldZeroIndex,newZeroIndex[count],perm)
			Q_2 <- pivotQ_2(oldZeroIndex,newZeroIndex[count],Q_2)
			v <- pivotP(oldZeroIndex,newZeroIndex[count],v)
			return(cycleMinNonGeneric(perm,v,Q_2,bC,count+1,replacementIndex))
		}
	}	
}

findDirections <- function(Q_2,v,bC,direction="Min"){
	n <- nrow(Q_2)
	m <- n - ncol(Q_2)
	if(direction == "Min"){
		feasibleDirections <- which(Q_2[bC,] < 0)
	}else{
		feasibleDirections <- which(Q_2[bC,] > 0)
	}
	if(length(which(v[1:m]==0)) == 0) return(feasibleDirections)
	notUseful <- NULL
	for(i in 1:length(feasibleDirections)){
		if(length(which(Q_2[,feasibleDirections] < 0 & v == 0)) != 0){
			notUseful <- c(notUseful,i)
		}
	}
	if(length(notUseful) != 0){
		feasibleDirections <- feasibleDirections[-notUseful]
	}
	return(feasibleDirections)
}
