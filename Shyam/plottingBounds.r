
for(i in 1:20){
x <- data.frame(runSim(1,5,5))

colnames(x) <- c("Coordinate","Max","Min","Fixed Value")

Large <- x$Max - x$Min

Small <- x$Min

par(mar=c(5,3,4,1)) 
barplot(rbind(Small,Large),
  xlab="Max and Min of 1st Entry",names.arg=factor(x$Coordinate), col=c("darkblue","red"))

dev.copy(jpeg,paste(i,".jpg"))
dev.off()

}